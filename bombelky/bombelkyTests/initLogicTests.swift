//
//  initLogicTests.swift
//  bombelky
//
//  Created by Grzegorz Moscichowski on 16/05/16.
//  Copyright © 2016 Grzegorz Moscichowski. All rights reserved.
//

import XCTest

class CanYouTestMe {
    var object : ComplexObjectGraph
    init(input:Int) {
        let number = input*3 /// code difficult to test
        object = type(of: self).prepareComplexObjectGraph(number)
    }
    class func prepareComplexObjectGraph(_ number:Int) -> ComplexObjectGraph {
        return ComplexObjectGraph(number: number)
    }
}

class ComplexObjectGraph {
    init(number:Int) {
        /// do something with "number" and forget about it
    }
}

class FakeCanYouTestMe:CanYouTestMe {
    static var generatedNumber:Int?
    override class func prepareComplexObjectGraph(_ number:Int) -> ComplexObjectGraph {
        generatedNumber = number
        return super.prepareComplexObjectGraph(number)
    }
}

class CanYouTestMeTests: XCTestCase {
    func testInitMethod() {
        let object = FakeCanYouTestMe(input:3)
        XCTAssertNotNil(object)
        XCTAssertEqual(FakeCanYouTestMe.generatedNumber!, 3*3)
    }
}
