//
//  TwoPlayerGameViewController.swift
//  bombelky
//
//  Created by Grzegorz Moscichowski on 07/06/16.
//  Copyright © 2016 Grzegorz Moscichowski. All rights reserved.
//

import Foundation
import UIKit

class TwoPlayerGameViewController: MultiplayerGameViewController {
    @IBOutlet weak var upsideDownBoard: UIView!
    var head2headOption = false
    override func viewDidLoad() {
        super.viewDidLoad()
        if head2headOption {
            upsideDownBoard.transform = CGAffineTransform(rotationAngle: .pi)
        }
        connectOpponents()
    }
    func connectOpponents() {
        let gvc1 = gameViewControllers[0]
        let gvc2 = gameViewControllers[1]
        gvc1.gameController?.opponentGameController = gvc2.gameController
        gvc2.gameController?.opponentGameController = gvc1.gameController
        
    }
}
