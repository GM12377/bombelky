//
//  FakeGameBoardDisplay.swift
//  Bomble
//
//  Created by Grzegorz Moscichowski on 13/05/16.
//  Copyright © 2016 Grzegorz Moscichowski. All rights reserved.
//

import Foundation
import UIKit

class FakeGameBoardDisplay: GameBoardDisplay {
    var addedDrops = [Drop]()
    var dropLocations = [CGPoint]()
    var boardSize:IntVector?
    
    func addDrop(_ drop: Drop) {
        addedDrops.append(drop)
        dropLocations.append(drop.position.cgPoint)
    }
    func removeDropByTag(_ tag: String?) {
        if let t = tag {
            addedDrops = addedDrops.filter{ $0.tag != t }
        }
    }
    func moveDrop(_ tag: String?, to position: IntVector) {
        if let i = indexOfDrop(tag) {
            addedDrops[i].position = position
            dropLocations[i] = position.cgPoint
        }
    }
    func moveDrop(_ tag: String?, to position: CGPoint) {
        if let i = indexOfDrop(tag) {
            addedDrops[i].position = IntVector(CGPoint(x: round(position.x), y: trunc(position.y)))
            dropLocations[i] = position
        }
    }
    func refreshSpillForDrop(_ drop: Drop) {
        if let i = indexOfDrop(drop.tag) {
            addedDrops[i].spillDirections = drop.spillDirections
        }
    }
    func spillDirectionsOfDrop(_ tag:String?) -> Set<DropSpillDirections>? {
        if let i = indexOfDrop(tag) {
            return addedDrops[i].spillDirections
        } else {
            return nil
        }
    }
    func indexOfDrop(_ tag:String?) -> Int? {
        let tags = addedDrops.map({$0.tag})
        return tags.index(where: {$0==tag})
    }
    func generateDescription() -> String {
        var result = [String]()
        if let displayedBoardSize = boardSize {
            for y in (0..<displayedBoardSize.y).reversed() {
                for x in 0..<displayedBoardSize.x {
                    result.append( Drop.letterFromDrop(dropOnPosition(IntVector(x,y))) )
                }
                if y>0 {
                    result.append(" ")
                }
            }
        }
        return result.reduce("") {$0+$1}
    }
    fileprivate func dropOnPosition(_ position:IntVector) -> Drop? {
        if let dropIndex = (addedDrops.index{$0.position == position}) {
            return addedDrops[dropIndex]
        }
        return nil
    }
}
