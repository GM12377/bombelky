//
//  PanGestureAnalysis.swift
//  bombelky
//
//  Created by Grzegorz Moscichowski on 25/05/16.
//  Copyright © 2016 Grzegorz Moscichowski. All rights reserved.
//

import Foundation
import UIKit

struct PanGestureAnalysis {
    var containerView:UIView?
    var previousDragVector:CGPoint?
    var moveVector = CGPoint(x: 0,y: 0)
    var slideDown = false
    var fingerUp = false
    
    mutating func processEventFrom(_ recognizer: UIPanGestureRecognizer) {
        if let containerView = containerView {
            _resetPreviousVectorIfNeeded(recognizer.state)
            _updateSlideDown(recognizer, containerView: containerView)
            _updateMoveVector(recognizer, containerView: containerView)
        }
    }
    
    mutating func _resetPreviousVectorIfNeeded(_ state: UIGestureRecognizerState) {
        switch state {
        case .changed:
            break
        case .ended, .cancelled, .failed:
            fingerUp = true
            break
        case .began, .possible:
            previousDragVector = CGPoint(x: 0,y: 0)
            fingerUp = false
        }
    }
    mutating func _updateMoveVector(_ recognizer: UIPanGestureRecognizer, containerView:UIView) {
        let recognizerVector = recognizer.translation(in: containerView)
        let moveDelta = recognizerVector - (previousDragVector ?? CGPoint.zero)
        previousDragVector = recognizerVector
        let dropsViewSize = containerView.bounds.size
        moveVector = CGPoint(x: moveDelta.x / dropsViewSize.width, y: -moveDelta.y / dropsViewSize.height)
    }
    mutating func _updateSlideDown(_ recognizer: UIPanGestureRecognizer, containerView:UIView) {
        let velocity = recognizer.velocity(in: containerView)
        let dropsViewSize = containerView.bounds.size
        let relativeVelocity = CGPoint(x: velocity.x / dropsViewSize.width, y: velocity.y / dropsViewSize.height)
        slideDown = (relativeVelocity.y > 10 * relativeVelocity.x) && (relativeVelocity.y > 1) && (recognizer.state == .ended)
    }
}
