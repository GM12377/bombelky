//
//  ActiveDropsContainerTests.swift
//  Bomble
//
//  Created by Grzegorz Moscichowski on 12/05/16.
//  Copyright © 2016 Grzegorz Moscichowski. All rights reserved.
//

import XCTest

class FakeBoard: BoardDropsContainer {
    var fallenDrops = [Drop]()
    var fallenIntoColumnEvents = [Int]()
    
    override func fallExistingDrop(_ drop: Drop, intoColumn column: Int) {
        fallenDrops.append(drop)
        fallenIntoColumnEvents.append(column)
    }
}

class ActiveDropsContainerTests: XCTestCase {
    var display = FakeGameBoardDisplay()
    var container : ActiveDropsContainer?
    var collisionDetector = FakeCollisionDetector()
    
    override func setUp() {
        super.setUp()
        container = ActiveDropsContainer(display: display,
                                         startCoords: IntVector(1, 2),
                                         dropStore:DropStore(colorGenerator: FakeColorGenerator()),
                                         collisionDetector: collisionDetector)
        _ = container!.addDrops()
    }
    
    //MARK: add drops
    func testAddDrops() {
        XCTAssertEqual(display.addedDrops.count, 2)
    }

    func testAddDropsOnPosition() {
        XCTAssertEqual(display.addedDrops[0].position, IntVector(1, 1))
        XCTAssertEqual(display.addedDrops[1].position, IntVector(1, 0))
    }
    func testAddDropsOnPositionNoSpace() {
        collisionDetector.alwaysCollide = true
        XCTAssertEqual(container!.addDrops(), false)
        XCTAssertEqual(display.addedDrops.count, 2)
    }

    func testAddDropsStarProperty() {
        XCTAssertEqual(display.addedDrops[0].star, true)
        XCTAssertEqual(display.addedDrops[1].star, false)
    }

   //MARK: fall drops
    func testFallDrops() {
        let board = FakeBoard(width: 3, dropStore: FakeDropStore())
        container!.fallDropsToContainer(board)
        XCTAssertEqual(board.fallenDrops.count, 2)
        XCTAssertEqual(board.fallenIntoColumnEvents, [1, 1])
    }
    func testFallDropsSequence() {
        let board = FakeBoard(width: 3, dropStore: FakeDropStore())
        container!.fallDropsToContainer(board)
        XCTAssert(board.fallenDrops[0].position.y<board.fallenDrops[1].position.y)
    }
    
    //MARK: move drops
    func testMoveDrops() {
        container!.moveByVector(CGPoint(x: 1,y: 0))
        XCTAssertEqual(display.addedDrops[0].position, IntVector(2,1))
    }
    func testMoveTwiceDrops() {
        XCTAssertEqual(display.addedDrops[0].position, IntVector(1,1))
        container!.moveByVector(CGPoint(x: 1,y: 0))
        container!.moveByVector(CGPoint(x: -1,y: 0))
        XCTAssertEqual(display.addedDrops[0].position, IntVector(1,1))
    }
    
    //MARK rotate drops
    func testRotate90() {
        container!.rotateRightByAngle(90)
        XCTAssertEqual(display.addedDrops[1].position, IntVector(0,1))
    }
    func testRotateMinus90() {
        container!.rotateRightByAngle(-90)
        XCTAssertEqual(display.addedDrops[1].position, IntVector(2,1))
    }
    func testRotate180() {
        container!.rotateRightByAngle(180)
        XCTAssertEqual(display.addedDrops[1].position, IntVector(1,2))
    }
    func testRotate720pl90() {
        container!.rotateRightByAngle(720+90)
        XCTAssertEqual(display.addedDrops[1].position, IntVector(0,1))
    }
    
    // MARK collision
    func testMoveWithCollision() {
        collisionDetector.alwaysCollide = true
        container!.moveByVector(CGPoint(x: 0,y: -10))
        XCTAssertEqual(display.addedDrops[1].position, IntVector(1,0))
    }
    func testRotateWithCollision() {
        collisionDetector.alwaysCollide = true
        container!.rotateRightByAngle(90)
        XCTAssertEqual(display.addedDrops[1].position, IntVector(1,0))
    }
    // MARK init helpers
    func testStartCoordsCalculation() {
        XCTAssertEqual(ActiveDropsContainer.calculateStartCoords(GameOptions(boardSize: IntVector(6,12))), IntVector(3,12))
        XCTAssertEqual(ActiveDropsContainer.calculateStartCoords(GameOptions(boardSize: IntVector(5,12))), IntVector(2,12))
    }
    // MARK fluent moves
    func testMoveDropsFluent() {
        container!.moveByVector(CGPoint(x: 1, y: 0))
        XCTAssertEqual(container!.drops[0].position, IntVector(2,1))
    }
    func testMoveDropsFluentFraction() {
        container!.moveByVector(CGPoint(x: 1.5, y: -0.1))
        XCTAssertEqual(display.dropLocations[0], CGPoint(x: 2.5, y: 0.9))
    }
    func testMoveDropsHysteresis() {
        container!.moveByVector(CGPoint(x: 0.7, y: 0))
        XCTAssertEqual(display.dropLocations[0], CGPoint(x: 1.7, y: 1))
        let board = FakeBoard(width: 3, dropStore: FakeDropStore())
        container!.fallDropsToContainer(board)
        XCTAssertEqual(board.fallenIntoColumnEvents, [2, 2])
    }
    func testMoveEatCollisions() {
        collisionDetector.alwaysCollide = true
        let remaining = container!.moveByVector(CGPoint(x: -10, y: 0))
        XCTAssertEqual(remaining, CGPoint(x: 0, y: 0))
    }
    func testMoveDownNoCollision() {
        let remaining = container!.moveByVector(CGPoint(x: 0, y: -1))
        XCTAssertEqual(remaining, CGPoint(x: 0, y: 0))
    }
    func testMoveDownNoCollisionFraction() {
        let remaining = container!.moveByVector(CGPoint(x: 0, y: -0.1))
        XCTAssertEqual(remaining, CGPoint(x: 0, y: 0.9))
        XCTAssertEqual(display.dropLocations[0], CGPoint(x: 1, y: 0.9))
        XCTAssertEqual(container!.drops[0].position, IntVector(1,0))
    }
    func testMoveDownCollisionFraction() {
        collisionDetector.alwaysCollide = true
        let remaining = container!.moveByVector(CGPoint(x: 0, y: -0.1))
        XCTAssertEqual(remaining, CGPoint(x: 0, y: -0.1))
        XCTAssertEqual(display.dropLocations[0], CGPoint(x: 1, y: 1))
        XCTAssertEqual(container!.drops[0].position, IntVector(1,1))
    }
}
