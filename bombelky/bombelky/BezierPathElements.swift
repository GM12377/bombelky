//
//  BezierPathElements.swift
//  bombelky
//
//  Created by Grzegorz Moscichowski on 24/05/16.
//  Copyright © 2016 Grzegorz Moscichowski. All rights reserved.
//

import Foundation
import UIKit

protocol BezierPathElement {
    func appendToPath(_ path:UIBezierPath)
    func rotated(_ degrees:Int) -> BezierPathElement
    func moved(_ vector:CGPoint) -> BezierPathElement
}

struct BezierLineData:BezierPathElement, Equatable {
    let startPoint:CGPoint
    let endPoint:CGPoint
    func appendToPath(_ path: UIBezierPath) {
        if path.isEmpty {
            path.move(to: startPoint)
        }
        path.addLine(to: endPoint)
    }
    func rotated(_ degrees:Int) -> BezierPathElement {
        return BezierLineData(startPoint: startPoint.rotatedBySqareAngle(-degrees), endPoint: endPoint.rotatedBySqareAngle(-degrees))
    }
    func moved(_ vector: CGPoint) -> BezierPathElement {
        return BezierLineData(startPoint: startPoint + vector, endPoint: endPoint + vector)
    }
}

func == (left: BezierLineData, right: BezierLineData) -> Bool {
    return  (left.startPoint == right.startPoint) &&
        (left.endPoint == right.endPoint)
}

struct BezierArcData:BezierPathElement, Equatable {
    let centerPoint:CGPoint
    let radius:CGFloat
    let startAngle:CGFloat
    let endAngle:CGFloat
    let clockwise:Bool
    func appendToPath(_ path: UIBezierPath) {
        path.addArc(withCenter: centerPoint,
                              radius: radius,
                              startAngle: startAngle.degreesToRadians,
                              endAngle: endAngle.degreesToRadians,
                              clockwise: clockwise)
    }
    func rotated(_ degrees:Int) -> BezierPathElement {
        let floatDeg = CGFloat(degrees)
        return BezierArcData(centerPoint:centerPoint.rotatedBySqareAngle(-degrees),
                             radius:radius,
                             startAngle:startAngle + floatDeg,
                             endAngle:endAngle + floatDeg,
                             clockwise:clockwise)
    }
    func moved(_ vector: CGPoint) -> BezierPathElement {
        return BezierArcData(centerPoint:centerPoint + vector, radius:radius, startAngle:startAngle, endAngle:endAngle, clockwise:clockwise)
    }
}

func == (left: BezierArcData, right: BezierArcData) -> Bool {
    return  (left.centerPoint == right.centerPoint) &&
        (left.radius == right.radius) &&
        (left.startAngle == right.startAngle) &&
        (left.endAngle == right.endAngle) &&
        (left.clockwise == right.clockwise) &&
        (left.centerPoint == right.centerPoint)
}
