//
//  BoardDropsActions.swift
//  bombelky
//
//  Created by Grzegorz Moscichowski on 13/05/16.
//  Copyright © 2016 Grzegorz Moscichowski. All rights reserved.
//

import Foundation

protocol BoardDropsContainerActions {
    func fallExistingDrop(_ drop:Drop, intoColumn column:Int)
    func fallFromSkyDrop(_ drop:Drop, intoColumn column:Int)
    func isEmptyLocation(_ location:IntVector) -> Bool
    func removeDropOnPosition(_ location:IntVector)
    func spillDrops()
    func blowBlobs() -> Bonus
    func addTrash(_ quantity:Int)
}

protocol BoardDropsContainerArchiving {
    func unarchiveBoardState(_ description:String)
    func archiveBoardState() -> String
}
