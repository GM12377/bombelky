//
//  Drop.swift
//  Bomble
//
//  Created by Grzegorz Moscichowski on 07/05/16.
//  Copyright © 2016 Grzegorz Moscichowski. All rights reserved.
//

import Foundation
import UIKit

enum DropColors:Int, CaseCountable {
    case colorA
    case colorB
    case colorC
    case colorD
    case colorE
    case colorF
    case colorG
    case trash
    static let caseCount = DropColors.countCases()
}

extension DropColors {
    init?(char: String) {
        switch char.lowercased() {
        case "a": self = .colorA
        case "b": self = .colorB
        case "c": self = .colorC
        case "d": self = .colorD
        case "e": self = .colorE
        case "f": self = .colorF
        case "g": self = .colorG
        case "#": self = .trash
        default: return nil
        }
    }
    func charRepresentation() -> String {
        switch self {
        case .colorA:    return "a"
        case .colorB:    return "b"
        case .colorC:    return "c"
        case .colorD:    return "d"
        case .colorE:    return "e"
        case .colorF:    return "f"
        case .colorG:    return "g"
        case .trash:     return "#"
        }
    }
    func UIColorRepresentation() -> UIColor {
        switch self {
        case .colorA:    return UIColor.red
        case .colorB:    return UIColor.green
        case .colorC:    return UIColor.blue
        case .colorD:    return UIColor.cyan
        case .colorE:    return UIColor.yellow
        case .colorF:    return UIColor.magenta
        case .colorG:    return UIColor.orange
        case .trash:     return UIColor.lightGray
        }
    }
}

enum DropSpillDirections:Int {
    case east 
    case se
    case south
    case sw
    case west
    case nw
    case north
    case ne
};

struct Drop:Equatable,Hashable {
    var color = DropColors.colorA
    var position = IntVector()
    var star = false
    var tag:String? = nil
    var spillDirections = Set<DropSpillDirections>()
    
    init() {}
    init(color: DropColors) {
        self.color = color
    }
    init(tag: String?) {
        self.tag = tag
    }
    init(position: IntVector) {
        self.position = position
    }
    init(color: DropColors, star:Bool, tag: String?) {
        self.color = color
        self.star = star
        self.tag = tag
    }
    init(color: DropColors, position: IntVector, star:Bool, tag: String?) {
        self.color = color
        self.position = position
        self.star = star
        self.tag = tag
    }
    var descriptionLetter: String {
        get {
            let char = color.charRepresentation()
            if star {
                return char.uppercased()
            }
            return char
        }
    }
    static func letterFromDrop(_ drop:Drop?) -> String {
        if let drop = drop {
            return drop.descriptionLetter
        }
        return "."
    }
    //MARK: Hashable
    var hashValue : Int {
        get {
            return "\(position.x),\(position.y),\(tag ?? "")".hashValue
        }
    }

}

func == (left: Drop, right: Drop) -> Bool {
    return
        (left.color == right.color) &&
        (left.position == right.position) &&
        (left.star == right.star) &&
        (left.tag == right.tag) &&
        (left.spillDirections == right.spillDirections)
}
