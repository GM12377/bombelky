//
//  NextDrops.swift
//  bombelky
//
//  Created by Grzegorz Moscichowski on 08/06/16.
//  Copyright © 2016 Grzegorz Moscichowski. All rights reserved.
//

import Foundation

class NextDrops {
    var dropStore:DropStore
    var display:GameStatisticsDisplay
    fileprivate var dropsOnStage=[Drop]()
    init(dropStore:DropStore, display:GameStatisticsDisplay) {
        self.dropStore = dropStore
        self.display = display
    }
    func updateNextDrops() {
        for drop in dropsOnStage {
            display.removeDropByTag(drop.tag)
        }
        for i in 0...1 {
            var drop = dropStore.getNextDrop(i)!
            drop.position = IntVector(0,2-i)
            dropsOnStage.append(drop)
            display.addDrop(drop)
        }
    }
}
