//
//  FakeDropStore.swift
//  bombelky
//
//  Created by Grzegorz Moscichowski on 18/05/16.
//  Copyright © 2016 Grzegorz Moscichowski. All rights reserved.
//

import Foundation

class FakeDropStore: DropStore {
    init() {
        super.init(colorGenerator: FakeColorGenerator())
    }
    class func dropStoreWithFakeRandomGenerator() -> DropStore {
        return DropStore(colorGenerator: RandomColorGenerator(seed: 7, lastColor: .colorF))
    }
}
