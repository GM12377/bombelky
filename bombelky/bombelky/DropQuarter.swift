//
//  DropQuarter.swift
//  bombelky
//
//  Created by Grzegorz Moscichowski on 23/05/16.
//  Copyright © 2016 Grzegorz Moscichowski. All rights reserved.
//

import Foundation
import UIKit

protocol BezierPathNode {
    var elements:[BezierPathElement] {get}
}

struct DropQuarter:BezierPathNode {
    let dropRadius:CGFloat = 0.98
    let neighbours:[Bool]
    let radius:CGFloat
    let rotationDegrees:Int
    let center:CGPoint
    var elements:[BezierPathElement] {
        let subpaths = _rawSubPathsData()
        let rotatedSubShapes = subpaths.map{$0.rotated(rotationDegrees)}
        return rotatedSubShapes.map{$0.moved(center)}
    }
    func _rawSubPathsData() -> [BezierPathElement] {
        if !neighbours[1] {
            return _fullArc()
        }
        var result = [BezierPathElement]()
        if !neighbours[0] {
            result.append(_firstSmallArc())
        }
        result.append(_lineCutFrom(!neighbours[0], second: !neighbours[2]))
        if !neighbours[2] {
            result.append(_secondSmallArc())
        }
        return result
    }
    func _firstSmallArc() -> BezierPathElement{
        return BezierArcData(centerPoint: CGPoint(x: radius, y: -radius),
            radius: radius * (sqrt(2)-dropRadius),
            startAngle: 90 + 45,
            endAngle: 90,
            clockwise: false)
    }
    func _secondSmallArc() -> BezierPathElement{
        return BezierArcData(centerPoint: CGPoint(x: radius, y: radius),
                             radius: radius * (sqrt(2)-dropRadius),
                             startAngle: 270,
                             endAngle: 270-45,
                             clockwise: false)
    }
    func _lineCutFrom(_ first:Bool, second:Bool) -> BezierPathElement {
        var firstCut:CGFloat = 0.0
        var secondCut:CGFloat = 0
        if first {
            firstCut = radius * (sqrt(2)-dropRadius)
        }
        if second {
            secondCut = radius * (sqrt(2)-dropRadius)
        }
        return BezierLineData(startPoint: CGPoint(x: radius, y: -radius+firstCut), endPoint: CGPoint(x: radius, y: radius-secondCut))
    }
    func _fullArc() -> [BezierPathElement] {
        return [BezierArcData(centerPoint: CGPoint(x: 0, y: 0),
            radius: radius * dropRadius,
            startAngle: -45,
            endAngle: 45,
            clockwise: true)]
    }
}

struct DropPathData:BezierPathNode {
    let neighbours: Set<DropSpillDirections>
    let radius:CGFloat
    let center:CGPoint
    var elements:[BezierPathElement] {
        var result = [BezierPathElement]()
        for i in 0...3 {
            let angle = i * 90
            let stickyNeighbours = [neighbourForAngle(angle - 90) && neighbourForAngle(angle - 45),
                                    neighbourForAngle(angle),
                                    neighbourForAngle(angle + 45) && neighbourForAngle(angle + 90)]
            let quarter = DropQuarter(neighbours: stickyNeighbours,
                                      radius: radius,
                                      rotationDegrees: angle,
                                      center: center)
            result += quarter.elements
        }
        return result
    }
    func neighbourForAngle(_ angle:Int) -> Bool {
        var index = angle / 45
        index = (index + 8) % 8
        let direction = DropSpillDirections(rawValue: index)
        if let direction = direction {
            return neighbours.contains(direction)
        }
        return false
    }
    func generatePath() -> UIBezierPath {
        let path = UIBezierPath()
        for shape in elements {
            shape.appendToPath(path)
        }
        path.close()
        return path
    }
}
