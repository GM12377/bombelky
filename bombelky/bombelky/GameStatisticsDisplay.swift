//
//  GameStatisticsDisplay.swift
//  bombelky
//
//  Created by Grzegorz Moscichowski on 08/06/16.
//  Copyright © 2016 Grzegorz Moscichowski. All rights reserved.
//

import Foundation

import UIKit

///
protocol GameStatisticsDisplay {
    func addDrop(_ drop:Drop)
    func removeDropByTag(_ tag: String?)
//    func moveDrop(tag:String?, to:IntVector)
//    func setPointsCounter(points:Int)
}
