//
//  BoardDropsDataTests.swift
//  bombelky
//
//  Created by Grzegorz Moscichowski on 17/05/16.
//  Copyright © 2016 Grzegorz Moscichowski. All rights reserved.
//

import XCTest

class BoardDropsDataTests: XCTestCase {

    func testGetByTag() {
        var dropsData = BoardDropsData(width: 3)
        let drop = Drop(tag: "2")
        _ = dropsData.addDrop(drop, onTopOfColumn: 2)
        XCTAssertEqual(dropsData.getDrop("2")?.color, drop.color)
    }

}
