//
//  UIView+ConstraintsHelper.swift
//  bombelky
//
//  Created by Grzegorz Moscichowski on 27/05/16.
//  Copyright © 2016 Grzegorz Moscichowski. All rights reserved.
//

import Foundation
import UIKit

extension UIView {
    func addAllEdgesConstraintsForSubView(_ subView:UIView) {
    subView.translatesAutoresizingMaskIntoConstraints = false
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[subView]|", options: .alignAllLeft, metrics: nil, views: ["subView": subView]))
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[subView]|", options: .alignAllLeft, metrics: nil, views: ["subView": subView]))
    }

}
