
import Foundation
import UIKit

/// Contains drops that are currently under user control - "active drops"; Allows operations on them
class ActiveDropsContainer:DropsContainer {
    var display:GameBoardDisplay?
    var dropStore:DropStore
    var startCoords:IntVector
    var collisionDetector:CollisionDetector
    var floatMoveAddition=CGPoint.zero
    var drops = [Drop]()
    
    // MARK: init
    init(display:GameBoardDisplay, startCoords:IntVector, dropStore:DropStore, collisionDetector:CollisionDetector) {
        self.display = display
        self.startCoords = startCoords
        self.dropStore = dropStore
        self.collisionDetector = collisionDetector
    }
    class func calculateStartCoords(_ options:GameOptionsBoardSize) -> IntVector {
        return IntVector(options.boardSize.x/2, options.boardSize.y)
    }
    
    // MARK: interface
    func addDrops() -> Bool {
        if collisionDetector.cropPathFrom(startCoords, vector: IntVector(0,-1)) == IntVector() {
            return false
        }
        var initialMove = -1
        if collisionDetector.cropPathFrom(startCoords + IntVector(0,-1), vector: IntVector(0,-1)) == IntVector() {
            initialMove = 0
        }
        var drops = dropStore.getActiveDrops()
        drops.first.position = IntVector(x: startCoords.x, y:startCoords.y + initialMove)
        drops.second.position = IntVector(x: startCoords.x, y:startCoords.y + initialMove - 1)
        _addDrop(drops.first)
        _addDrop(drops.second)
        return true
    }
    fileprivate func _addDrop(_ drop:Drop) {
        drops.append(drop)
        self.display?.addDrop(drop)
    }
    
    func fallDropsToContainer(_ container: BoardDropsContainer) {
        let sortedDrops = drops.sorted{$0.position.y < $1.position.y}
        for drop in sortedDrops {
            container.fallExistingDrop(drop, intoColumn: drop.position.x)
        }
        drops.removeAll()
    }
    @discardableResult func moveByVector(_ vector: CGPoint) -> CGPoint {
        let positions = drops.map{$0.position}
        let longestVector = collisionDetector.cropPathFrom(positions, vector: vector)
        let eatenVector = _hysteresis(longestVector)
        floatMoveAddition = longestVector - eatenVector
        for i in 0..<drops.count {
            drops[i].position = drops[i].position + IntVector(eatenVector)
            self.display?.moveDrop(drops[i].tag, to: drops[i].position.cgPoint + floatMoveAddition)
        }
        let dxForNextTime = longestVector.x - eatenVector.x
        let dyForNextTime = vector.y - eatenVector.y
         return CGPoint(x: dxForNextTime, y: dyForNextTime)
    }
    func _hysteresis(_ vector:CGPoint) -> CGPoint {
        var result = CGPoint(x: trunc(vector.x), y: floor(vector.y))
        let hysteresis:CGFloat = 0.6
        if (vector.x > 0) && (floor(vector.x) + hysteresis < vector.x ) {
            result.x = ceil(vector.x)
        }
        if (vector.x < 0) && (ceil(vector.x) - hysteresis > vector.x ) {
            result.x = floor(vector.x)
        }
        return result
    }
    
    func rotateRightByAngle(_ degrees:Int) {
        let satelliteVector = drops[1].position - drops[0].position
        let rotatedVector = satelliteVector.rotatedBySqareAngle(degrees)
        let longestVector = collisionDetector.cropPathFrom(drops[0].position, vector: rotatedVector)
        if (longestVector == rotatedVector) {
            drops[1].position = drops[0].position + rotatedVector
            self.display?.moveDrop(drops[1].tag, to: drops[1].position.cgPoint + floatMoveAddition)
        }
    }
    func removeAllDrops() {
        for drop in drops {
            self.display?.removeDropByTag(drop.tag)
        }
        drops.removeAll()
    }
}
