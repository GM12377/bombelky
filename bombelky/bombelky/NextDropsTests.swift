//
//  NextDropsTests.swift
//  bombelky
//
//  Created by Grzegorz Moscichowski on 08/06/16.
//  Copyright © 2016 Grzegorz Moscichowski. All rights reserved.
//

import XCTest

class NextDropsTests: XCTestCase {

    func testGetAndDisplayNextDrops() {
        let display = FakeStatisticsDisplay()
        let dropStore = FakeDropStore.dropStoreWithFakeRandomGenerator()
        let nextDrops = NextDrops(dropStore: dropStore, display: display)
        nextDrops.updateNextDrops()
        XCTAssertEqual(display.generateDescription(), ". D d .")
    }
}
