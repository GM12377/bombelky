//
//  BoardDropsContainerTests.swift
//  bombelky
//
//  Created by Grzegorz Moscichowski on 13/05/16.
//  Copyright © 2016 Grzegorz Moscichowski. All rights reserved.
//

import XCTest

class BoardDropsContainerTests: XCTestCase {
    var container : FakeBoardDropsContainer = FakeBoardDropsContainer()
    func testFall() {
        container.fallTo20()
        container.fallTo20()
        XCTAssertEqual(container.fakeDisplay.addedDrops.count, 2)
        XCTAssertEqual(container.fakeDisplay.addedDrops[0].position, IntVector(2, 0))
        XCTAssertEqual(container.fakeDisplay.addedDrops[1].position, IntVector(2, 1))
    }

    func testIsEmpty() {
        XCTAssertEqual(container.isEmptyLocation(IntVector(2,0)), true)
        container.fallTo20()
        XCTAssertEqual(container.isEmptyLocation(IntVector(2,0)), false)
        XCTAssertEqual(container.isEmptyLocation(IntVector(1,0)), true)
    }
    func testIsEmptyDropTwice() {
        container.fallTo20()
        container.fallTo20()
        XCTAssertEqual(container.isEmptyLocation(IntVector(2,0)), false)
        XCTAssertEqual(container.isEmptyLocation(IntVector(2,1)), false)
    }
    func testFallRemove() {
        container.fallTo20()
        container.removeDropOnPosition(IntVector(2,0))
        XCTAssertEqual(container.isEmptyLocation(IntVector(2,0)), true)
    }
    func testFallRemoveNonExisting() {
        container.removeDropOnPosition(IntVector(2,0))
    }

    func testOutOfRange() {
        container.fallFromSkyDrop(Drop(position:IntVector(1, 1)), intoColumn:3)
        XCTAssertEqual(container.fakeDisplay.addedDrops.count, 0)
        container.removeDropOnPosition(IntVector(20,20))
        _ = container.isEmptyLocation(IntVector(20,20))
        _ = container.isEmptyLocation(IntVector(-1,0))
    }
    func testDisplayUnarchiving() {
        let input = "... .B."
        container.unarchiveBoardState(input)
        XCTAssert(container.fakeDisplay.addedDrops.first?.position == IntVector(1,0))
    }
    func testRemoveAll() {
        let input = "aaa BBB"
        container.unarchiveBoardState(input)
        XCTAssertEqual(container.fakeDisplay.addedDrops.count, 6)
        container.removeAllDrops()
        XCTAssertEqual(container.fakeDisplay.addedDrops.count, 0)
    }
    
    //MARK blobs
    func testBlowBlobs() {
        let input = "... .B. AB#"
        container.unarchiveBoardState(input)
        XCTAssertEqual(container.archiveBoardState(),"... .B. AB#")
        container.blowBlobs()
        XCTAssertEqual(container.archiveBoardState(), "... A..")
        XCTAssertEqual(container.fakeDisplay.addedDrops.count, 1)
    }
    func testFallAfterBlowBlobs() {
        let input = "... .BA .B#"
        container.unarchiveBoardState(input)
        container.blowBlobs()
        XCTAssertEqual(container.archiveBoardState(), "... ..A")
        XCTAssert(container.fakeDisplay.addedDrops.first?.position == IntVector(2,0))
    }
    

}
