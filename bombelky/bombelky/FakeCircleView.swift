//
//  CircleView.swift
//  bombelky
//
//  Created by Grzegorz Moscichowski on 20/05/16.
//  Copyright © 2016 Grzegorz Moscichowski. All rights reserved.
//

import Foundation
import UIKit

@IBDesignable
class CircleView: UIView {
    var shapes: [CAShapeLayer]?
    var color: UIColor?
    var name: String?
    var star = false
    
    override func layoutSubviews() {
        super.layoutSubviews()
        if shapes == nil {
            shapes = [circle()]
            if star {
                shapes?.append(starShape())
            }
            for shape in shapes! {
                layer.addSublayer(shape)
            }
        }
    }
    func circle() -> CAShapeLayer {
        return DropLayer(frame:bounds, color: color, neighbours: Set([.north]))
    }
    func starShape() -> CAShapeLayer {
        let layer = CAShapeLayer()
        let fillColor = UIColor.black
        layer.fillColor = fillColor.cgColor
        let center = CGPoint(x: bounds.size.width/2, y: bounds.size.height/2)
        let path = UIBezierPath(arcCenter: center,
                                radius: bounds.size.width/4,
                                startAngle: 0,
                                endAngle: .pi*2,
                                clockwise: true)
        path.close()
        layer.path = path.cgPath
        return layer
    }

}
