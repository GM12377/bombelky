//
//  DropLayer.swift
//  bombelky
//
//  Created by Grzegorz Moscichowski on 24/05/16.
//  Copyright © 2016 Grzegorz Moscichowski. All rights reserved.
//

import Foundation
import UIKit

class StarLayer: CAShapeLayer {
    init(frame:CGRect) {
        super.init()
        anchorPoint = CGPoint(x: 0, y: 0)
        self.frame = frame
        fillColor = UIColor.black.cgColor
        let center = CGPoint(x: frame.size.width/2, y: frame.size.height/2)
        let star = UIBezierPath(arcCenter: center,
                                radius: frame.size.width/4,
                                startAngle: 0,
                                endAngle: .pi*2,
                                clockwise: true)
        star.close()
        path = star.cgPath
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}

class DropLayer: CAShapeLayer {
    init(frame:CGRect, color: UIColor?, neighbours:Set<DropSpillDirections>) {
        super.init()
        anchorPoint = CGPoint(x: 0, y: 0)
        self.frame = frame
        fillColor = (color ?? (UIColor.red)).cgColor
        strokeColor = fillColor
        lineWidth = 0.5
        let dropShape = DropPathData(neighbours: neighbours,
                                     radius: frame.size.width/2,
                                     center: CGPoint(x: frame.size.width/2, y: frame.size.width/2))
        path = dropShape.generatePath().cgPath
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    override init(layer: Any) {
        super.init(layer: layer)
    }

}
