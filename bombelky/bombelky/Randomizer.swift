//
//  Randomizer.swift
//  bombelky
//
//  Created by Grzegorz Moscichowski on 15/05/16.
//  Copyright © 2016 Grzegorz Moscichowski. All rights reserved.
//

import Foundation

struct Randomizer {
    var state = [Int64](repeating: 0, count: 344)
    init(seed:Int) {
        var r = [Int64](repeating: 0, count: 344)
        r[0] = Int64(seed)
        for i in 1..<31 {
            r[i] = (16807 * r[i-1]) % 0x7FFFFFFF
        }
        for i in 31..<34 {
            r[i] = r[i-31];
        }
        for i in 34..<344 {
            r[i] = (r[i-31] + r[i-3]) & 0xffffffff
        }
        for i in 344-31..<344 {
            state[i-344+31] = r[i]
        }
    }

    mutating func rand(_ max:Int) -> Int {
        return rand() % max
    }
    mutating func rand() -> Int {
        let i = 31
        state[i] = (state[i-31] + state[i-3]) & 0xffffffff
        let result = Int(state[i] >> 1)
        for i in 0..<32 {
            state[i] = state[i+1];
        }
        return result;
    }
    
}
