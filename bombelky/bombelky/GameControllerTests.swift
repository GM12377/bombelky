//
//  GameBoardTests.swift
//  bombelky
//
//  Created by Grzegorz Moscichowski on 15/05/16.
//  Copyright © 2016 Grzegorz Moscichowski. All rights reserved.
//

import XCTest

class FakeGameController:GameController {
    func startGame() {
        
    }
    func insertTrash(_ trash:Int) {
        trashInserted = trash
    }
    weak var opponentGameController:GameController?
    var trashInserted:Int?
}

class GameControllerTests: XCTestCase {
    var gameController:GameControllerImp?
    let fakeDisplay = FakeGameBoardDisplay()
    let fakeStatisticsDisplay = FakeStatisticsDisplay()

    func testReadyToStart() {
        let options = GameOptionsFactory.test6_12game()
        let gc = GameControllerImp(display: fakeDisplay, statisticsDisplay: fakeStatisticsDisplay, options:options)
        XCTAssert(gc.gameState == .readyToStart)
    }
    
    func testStarted() {
        start6_12game()
        XCTAssert(gameController!.gameState == .started)
    }
    func testDisplayClean() {
        start6_12game()
        XCTAssertEqual(fakeDisplay.boardSize, IntVector(6,12))
    }
    func start6_12game() {
        let options = GameOptionsFactory.test6_12game()
        gameController = GameControllerImp(display: fakeDisplay, statisticsDisplay: fakeStatisticsDisplay, options:options)
        gameController!.startGame()
    }
    func start4_4game() {
        prepare4_4game()
        gameController!.startGame()
    }
    func prepare4_4game() {
        let options = GameOptionsFactory.test4_4game()
        gameController = GameControllerImp(display: fakeDisplay, statisticsDisplay: fakeStatisticsDisplay, options:options)
    }
    func testFirstActiveDrops() {
        start6_12game()
        XCTAssertEqual(fakeDisplay.addedDrops.count, 2)
        XCTAssertEqual(fakeDisplay.addedDrops[0].position, IntVector(3,11))
        XCTAssertEqual(fakeDisplay.addedDrops[1].position, IntVector(3,10))
        XCTAssertEqual(fakeDisplay.addedDrops[0].star, true)
        XCTAssertEqual(fakeDisplay.addedDrops[1].star, false)
    }
    func testFallActiveDrops() {
        start4_4game()
        gameController!.fall()
        XCTAssertEqual(fakeDisplay.addedDrops.count, 4)
        XCTAssertEqual(gameController!.boardDrops.data.generateDescription(),
            ".... " +
            "..D. " +
            "..d.")
        XCTAssertEqual(fakeDisplay.generateDescription(),
            "..F. " +
            "..b. " +
            "..D. " +
            "..d.")
    }
    func testGameOver() {
        start4_4game()
        gameController!.fall()
        gameController!.fall()
        XCTAssertEqual(gameController!.boardDrops.data.generateDescription(), ".... ..F. ..b. ..D. ..d.")
        XCTAssertEqual(fakeDisplay.generateDescription(), "..F. ..b. ..D. ..d.")
        XCTAssert(gameController!.gameState == .gameOver)
    }
    func testStickToGround() {
        start4_4game()
        gameController!.move(CGPoint(x: 0, y: -2))
        gameController!.move(CGPoint(x: 0, y: -2))
        XCTAssertEqual(gameController!.boardDrops.data.generateDescription(), ".... ..F. ..b. ..D. ..d.")
        XCTAssertEqual(fakeDisplay.generateDescription(), "..F. ..b. ..D. ..d.")
        XCTAssert(gameController!.gameState == .gameOver)
    }
    func testNotStickToBorder() {
        start4_4game()
        gameController!.move(CGPoint(x: 10, y: 0))
        XCTAssertEqual(gameController!.boardDrops.data.generateDescription(), "....")
        XCTAssertEqual(fakeDisplay.generateDescription(), "...D ...d .... ....")
    }
    func testBlowBlobs() {
        start4_4game()
        gameController!.boardDrops.unarchiveBoardState(".... c..B #..D dddB")
        gameController!.fall()
        XCTAssertEqual(fakeDisplay.generateDescription(), "..F. ..b. .... c...")
    }
    func testAcumulateMoves() {
        start4_4game()
        XCTAssertEqual(fakeDisplay.addedDrops[0].position, IntVector(2,3))
        gameController!.move(CGPoint(x: -0.5/4, y: -0.5/4))
        gameController!.move(CGPoint(x: -0.5/4, y: -0.5/4))
        XCTAssertEqual(fakeDisplay.addedDrops[0].position, IntVector(1,2))
    }
    func testNotAcumulateColisionMoves() {
        start4_4game()
        XCTAssertEqual(fakeDisplay.addedDrops[0].position, IntVector(2,3))
        gameController!.move(CGPoint(x: -10/4, y: 0))
        gameController!.move(CGPoint(x: +0.7/4, y: 0))
        XCTAssertEqual(fakeDisplay.addedDrops[0].position, IntVector(1,3))
    }    
    func testIssueWithOverlapingDrops() {
        start4_4game()
        gameController!.boardDrops.unarchiveBoardState("..#.")
        gameController!.move(CGPoint(x: +0.01/4, y: -1.2/4))
        XCTAssertEqual(fakeDisplay.generateDescription(), "..b. ..D. ..d. ..#.")
    }
    func testIssueWithStickToWrongColumn() {
        start4_4game()
        gameController!.move(CGPoint(x: 0/4, y: -10))
        XCTAssertEqual(fakeDisplay.generateDescription(), "..F. ..b. ..D. ..d.")
        gameController!.move(CGPoint(x: +0.7/4, y: 0/4))
        gameController!.move(CGPoint(x: 0/4, y: -0.1/4))
        XCTAssertEqual(fakeDisplay.generateDescription(), ".... ...F ..Db ..d.")
        gameController!.move(CGPoint(x: 0/4, y: -2.0/4))
        XCTAssertEqual(fakeDisplay.generateDescription(), "..F. ..d. ..DF ..db")
    }
    func testDoubleCheckWhenJumpungInHole() {
        start4_4game()
        gameController!.move(CGPoint(x: 0/4, y: -10))
        XCTAssertEqual(fakeDisplay.generateDescription(), "..F. ..b. ..D. ..d.")
        gameController!.rotate(90)
        gameController!.move(CGPoint(x: 10/4, y: -10/4))
        XCTAssertEqual(fakeDisplay.generateDescription(), "..d. ..b. ..D. ..dF")
    }
    func testDoubleCheckWhenJumpungInHole2() {
        start4_4game()
        gameController!.move(CGPoint(x: 0/4, y: -10))
        XCTAssertEqual(fakeDisplay.generateDescription(), "..F. ..b. ..D. ..d.")
        gameController!.rotate(-90)
        gameController!.move(CGPoint(x: 10/4, y: -10/4))
        XCTAssertEqual(fakeDisplay.generateDescription(), "..d. ..F. ..D. ..db")
    }
    
    //MARK: multiplayer
    func testSendTrashToOpponent() {
        start4_4game()
        let gameController2 = FakeGameController()
        gameController!.opponentGameController = gameController2
        gameController!.boardDrops.unarchiveBoardState(".... AAaa aaaa")
        gameController!.fall()
        XCTAssertEqual(gameController2.trashInserted, 9)
    }
    func trashInDescription(_ description:String) -> Int {
        return description.reduce(0){ (total, char) in
            if char == "#" {
                return total + 1
            }
            return total
        }
    }
    func testReceiveTrash() {
        start4_4game()
        gameController!.insertTrash(1)
        XCTAssertEqual(fakeDisplay.generateDescription(), "..D. ..d. .... ....")
        gameController!.fall()
        XCTAssertEqual(trashInDescription(fakeDisplay.generateDescription()), 1)
   }
    func testReceiveTrashCumulation() {
        start4_4game()
        gameController!.insertTrash(1)
        gameController!.insertTrash(3)
        gameController!.fall()
        XCTAssertNotEqual(fakeDisplay.generateDescription(), "#.b. #... #.D. #.d.")
        XCTAssertEqual(trashInDescription(fakeDisplay.generateDescription()), 4)
    }
    func testReceiveTrashColumnRandomization() {
        start4_4game()
        gameController!.insertTrash(100)
        gameController!.fall()
        XCTAssertEqual(fakeDisplay.generateDescription(), "#### #### ##D# ##d#")
    }

    //MARK: next drops
    func testDisplayNextDrops() {
        start4_4game()
        XCTAssertEqual(fakeStatisticsDisplay.generateDescription(), ". F b .")
        gameController?.fall()
        XCTAssertEqual(fakeStatisticsDisplay.generateDescription(), ". F d .")
    }
    // MARK: initial 1 down move if possible
    func testInitialMoveCollisionGO() {
        start4_4game()
        gameController!.boardDrops.unarchiveBoardState("#### ####")
        gameController!.fall()
        XCTAssert(gameController!.gameState == .gameOver)
    }
    func testInitialMoveCollision() {
        start4_4game()
        gameController!.boardDrops.unarchiveBoardState("####")
        gameController!.fall()
        XCTAssertEqual(fakeDisplay.generateDescription(), "..b. ..D. ..d. ####")
    }
    func testInitialMoveNoCollision() {
        start4_4game()
        gameController!.fall()
        XCTAssertEqual(fakeDisplay.generateDescription(), "..F. ..b. ..D. ..d.")
    }
    func testFingerUp() {
        start4_4game()
        gameController!.move(CGPoint(x: 0.7/4, y: 0))
        XCTAssertEqual(fakeDisplay.dropLocations[0], CGPoint(x: 2.7, y: 3))
        gameController!.fingerUp()
        XCTAssertEqual(fakeDisplay.dropLocations[0], CGPoint(x: 3.0, y: 3))
    }

}

