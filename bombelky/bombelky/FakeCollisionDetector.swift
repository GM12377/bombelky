//
//  FakeCollisionDetector.swift
//  bombelky
//
//  Created by Grzegorz Moscichowski on 13/05/16.
//  Copyright © 2016 Grzegorz Moscichowski. All rights reserved.
//

import Foundation
import UIKit

class FakeCollisionDetector: CollisionDetector {
    var alwaysCollide = false
    func cropPathFrom(_ position:IntVector, vector:IntVector) -> IntVector {
        if alwaysCollide {
            return IntVector()
        }
        return vector
    }
    func cropPathFrom(_ positions: [IntVector], vector: CGPoint) -> CGPoint {
        if alwaysCollide {
            return CGPoint.zero
        }
        return vector
    }
    func cropPathFrom(_ positions: [IntVector], vector: IntVector) -> IntVector {
        return cropPathFrom(positions.first!, vector: vector)
    }
}
