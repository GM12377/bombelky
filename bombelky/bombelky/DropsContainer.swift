//
//  DropsContainer.swift
//  Bomble
//
//  Created by Grzegorz Moscichowski on 13/05/16.
//  Copyright © 2016 Grzegorz Moscichowski. All rights reserved.
//

import Foundation

protocol DropsContainer {
    var display:GameBoardDisplay? {get set}
    var dropStore:DropStore {get}
    func removeAllDrops()
}
