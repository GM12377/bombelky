//
//  DropBoardASCII.swift
//  bombelky
//
//  Created by Grzegorz Moscichowski on 17/05/16.
//  Copyright © 2016 Grzegorz Moscichowski. All rights reserved.
//

import Foundation

struct BoardDropsData {
    let width:Int
    var columns = [[Drop]]()
    
    init(width:Int) {
        self.width = width
        for _ in 0..<width {
            columns.append([Drop]())
        }
    }    
    mutating func addDrop(_ drop:Drop, onTopOfColumn column:Int) -> Drop? {
        if validLocation(IntVector(column,0)) {
            var d = drop
            d.position = IntVector(x: column,y: columns[column].count)
            columns[column].append(d)
            return d
        }
        return nil
    }
    func isEmptyLocation(_ location:IntVector) -> Bool {
        if !validLocation(location) {
            return false
        }
        return columns[location.x].count <= location.y
    }
    mutating func removeDropOnPosition(_ location:IntVector) {
        if validLocation(location) {
            if location.y < columns[location.x].count {
                columns[location.x].remove(at: location.y)
            }
        }
    }
    func validLocation(_ location: IntVector) -> Bool {
        return
            (location.x < width) &&
            (location.x >= 0) &&
            (location.y >= 0)
    }
    func getDrop(_ location:IntVector) -> Drop? {
        if !validLocation(location) {
            return nil
        }
        if columns[location.x].count > location.y {
            return columns[location.x][location.y]
        }
        return nil
    }
    func getDrop(_ tag:String?) -> Drop? {
        for column in columns {
            for drop in column {
                if drop.tag == tag {
                    return drop
                }
            }
        }
        return nil
    }
    mutating func removeDrops(_ drops:[Drop]) {
        let sortedDrops = drops.sorted{$0.position.y>$1.position.y}
        for drop in sortedDrops {
            columns[drop.position.x].remove(at: drop.position.y)
        }
    }
    mutating func removeAllDrops() -> [Drop] {
        var result = [Drop]()
        for (index, column) in columns.enumerated() {
            result += column
            columns[index].removeAll()
        }
        return result
    }
}
