//
//  RandomColorGeneratorTests.swift
//  bombelky
//
//  Created by Grzegorz Moscichowski on 16/05/16.
//  Copyright © 2016 Grzegorz Moscichowski. All rights reserved.
//

import XCTest

class RandomColorGeneratorTests: XCTestCase {

    func testExampleSeed() {
        let options = GameOptions(randomColorsSeed: 777, dropColorsNumber: 4)
        let generator = RandomColorGenerator(options: options)
        var colors = [DropColors]()
        for _ in 0...3 {
            colors.append(generator.getNextColor())
        }
        XCTAssertEqual(colors, [.colorD,.colorD,.colorA,.colorD])
    }
}
