//
//  BlobsSet.swift
//  bombelky
//
//  Created by Grzegorz Moscichowski on 18/05/16.
//  Copyright © 2016 Grzegorz Moscichowski. All rights reserved.
//

import Foundation

struct Blob {
    var drops = [Drop]()
    var starCount = 0
}

func + (left: Blob, right: Blob) -> Blob {
    return Blob(drops: left.drops + right.drops, starCount: left.starCount + right.starCount)
}

protocol BlobsData {
    var blobs: [Blob] {get}
    var fragileTrash: [Drop] {get}
    mutating func addPotentialDropsPair(_ drop:Drop, neighbour:Drop?)
    func blobIndexWith(_ tag:String?) -> Int?
    func allFragileDrops() -> [Drop]
}


struct BlobsDataImp:BlobsData {
    var blobs : [Blob]
    var fragileTrash : [Drop]
    init() {
        self.init(blobs:[Blob](), fragileTrash:[Drop]())
    }
    init(blobs:[Blob]) {
        self.init(blobs:blobs, fragileTrash:[Drop]())
    }
    init(blobs:[Blob], fragileTrash:[Drop]) {
        self.blobs = blobs
        self.fragileTrash = fragileTrash
    }
    mutating func addPotentialDropsPair(_ drop:Drop, neighbour:Drop?){
        if drop.color == .trash {
            return
        }
        if let second = neighbour {
            if drop.color == second.color {
                addDrops([drop, second])
            }
        }
    }
    fileprivate mutating func addDrops(_ drops:[Drop]) {
        let unwrappedTags = drops.flatMap{$0.tag}
        if unwrappedTags.count != 2 {
            return
        }
        let indexTuple = (blobIndexWith(unwrappedTags[0]), blobIndexWith(unwrappedTags[1])) 
        switch indexTuple {
        case (.none, .none):
            addToNewBlob(drops)
        case (.some(let index), .none):
            addToBlob(index, drop: drops[1])
        case (.none, .some(let index)):
            addToBlob(index, drop: drops[0])
        case (.some(let index1), .some(let index2)):
            unionBlobs((index1, index2))
        }
    }
    fileprivate mutating func unionBlobs(_ indexes:(Int, Int)) {
        if (indexes.0 == indexes.1) {
            return
        }
        blobs[indexes.0] = blobs[indexes.0] + blobs[indexes.1]
        blobs.remove(at: indexes.1)
    }
    fileprivate mutating func addToBlob(_ index:Int, drop:Drop) {
        blobs[index].drops.append(drop)
        blobs[index].starCount += starCount(drop)
    }
    fileprivate mutating func addToNewBlob(_ drops:[Drop]) {
        let starsCount = drops.reduce(0) { $0 + starCount($1) }
        let blob = Blob(drops: drops, starCount: starsCount)
        blobs.append(blob)
    }
    fileprivate func starCount(_ drop:Drop) -> Int {
        if drop.star {
            return 1
        }
        return 0
    }
    func blobIndexWith(_ tag:String?) -> Int? {
        if let tagString = tag {
            for (index, blob) in blobs.enumerated() {
                if (blob.drops.map{$0.tag!}).contains(tagString) {
                    return index
                }
            }
        }
        return nil
    }
    func allFragileDrops() -> [Drop] {
        var result = fragileTrash
        result += blobs.flatMap{$0.drops}
        return result
    }
}
