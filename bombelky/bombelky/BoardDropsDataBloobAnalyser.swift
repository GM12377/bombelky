//
//  BloobAnalyser.swift
//  bombelky
//
//  Created by Grzegorz Moscichowski on 14/05/16.
//  Copyright © 2016 Grzegorz Moscichowski. All rights reserved.
//

import Foundation

extension BoardDropsData {
    func spillDrops(_ callback:(_ drop: Drop) -> Void) {
        for column in columns {
            for drop in column {
                _spillNeighbors(drop, callback: callback)
            }
        }
    }
    fileprivate func _spillNeighbors(_ drop:Drop, callback:(_ drop: Drop) -> Void) {
        var neighbours = Set<DropSpillDirections>()
        let neighborVectors = _directions()
        for (vector, direction) in neighborVectors {
            let neighbour = getDrop(drop.position + vector)
            if let neighbour = neighbour {
                if drop.color == neighbour.color {
                    neighbours.insert(direction)
                }
            }
        }
        if drop.spillDirections != neighbours {
            /// TODO: update drop info
            var spilledDrop = drop
            spilledDrop.spillDirections = neighbours
            callback(spilledDrop)
        }
    }
    fileprivate func _directions() -> [IntVector: DropSpillDirections] {
        return [IntVector(-1,0): .west,
                IntVector(1,0): .east,
                IntVector(0,-1): .south,
                IntVector(0,1): .north,
                IntVector(-1,-1): .sw,
                IntVector(-1,1): .nw,
                IntVector(1,-1): .se,
                IntVector(1,1): .ne]
    }
    func findFragileBlobs() -> BlobsData {
        var blobsSet = BlobsDataImp()
        for (x, column) in columns.enumerated() {
            for (y, drop) in column.enumerated() {
                blobsSet.addPotentialDropsPair(drop, neighbour: getDrop(IntVector(x:x, y:y+1)))
                blobsSet.addPotentialDropsPair(drop, neighbour: getDrop(IntVector(x:x+1, y:y)))
            }
        }
        blobsSet.blobs = blobsSet.blobs.filter{$0.starCount > 1}
        var trashSet = Set<Drop>()
        
        for drop in (blobsSet.blobs.flatMap{$0.drops}) {
            for trash in _findFragileTrash(drop.position) {
                trashSet.insert(trash)
            }
        }
        blobsSet.fragileTrash = Array(trashSet)
        return blobsSet
    }
    fileprivate func _findFragileTrash(_ position:IntVector) -> [Drop] {
        var result = [Drop]()
        for direction in _directions().keys {
            let drop = getDrop(position + direction)
            if let drop = drop, drop.color == .trash {
                result.append(drop)
            }
        }
        return result
    }
    mutating func fallDrops() -> [Drop] {
        var modifiedDrops = [Drop]()
        for (x, column) in columns.enumerated() {
            for (y, drop) in column.enumerated() {
                let properCoords = IntVector(x,y)
                if (drop.position != properCoords) {
                    columns[x][y].position = properCoords
                    modifiedDrops.append(columns[x][y])
                }
            }
        }
        return modifiedDrops
    }
}
