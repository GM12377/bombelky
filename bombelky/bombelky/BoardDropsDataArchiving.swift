//
//  BoardDropsDataArchiving.swift
//  bombelky
//
//  Created by Grzegorz Moscichowski on 17/05/16.
//  Copyright © 2016 Grzegorz Moscichowski. All rights reserved.
//

import Foundation

extension BoardDropsData {
    @discardableResult mutating func loadDrops(_ description:String, dropStore:DropStore) -> [Drop] {
        var i = width
        var dropsAdded = [Drop]()
        for char in description.reversed() {
            i -= 1
            let letter = String(char)
            func _addDrop(_ star:Bool, color:DropColors) {
                let drop = Drop(color: DropColors(char: letter)!, star:star, tag:dropStore.tagGenerator.getNextTag())
                if let drop = addDrop(drop, onTopOfColumn: i) {
                    dropsAdded.append(drop)
                }
            }
            switch letter {
            case DropColors.colorA.charRepresentation().uppercased() ... DropColors.colorG.charRepresentation().uppercased():
                _addDrop(true, color: DropColors(char: letter)!)
            case DropColors.colorA.charRepresentation() ... DropColors.colorG.charRepresentation():
                _addDrop(false, color: DropColors(char: letter)!)
            case "#":
                _addDrop(false, color: .trash)
            case ".":
                break
            default:
                i = width
            }
        }
        return dropsAdded
    }
    func generateDescription() -> String {
        var result = [String]()
        var nextRow = true
        var y = 0
        while nextRow {
            nextRow = false
            for i in (0..<width).reversed() {
                let drop = getDrop(IntVector(i,y))
                result.append(letterFromDrop(drop))
                if drop != nil {
                    nextRow = true
                }
            }
            if (nextRow) {
                result.append(" ")
            }
            y+=1
        }
        return result.reversed().reduce("") {$0+$1}
    }
    func letterFromDrop(_ drop:Drop?) -> String {
        if let drop = drop {
            return drop.descriptionLetter
//            let char = drop.color.charRepresentation()
//            if drop.star {
//                return char.uppercaseString
//            }
//            return char
        }
        return "."
    }
}
