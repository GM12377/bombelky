//
//  MenuViewController.swift
//  bombelky
//
//  Created by Grzegorz Moscichowski on 27/05/16.
//  Copyright © 2016 Grzegorz Moscichowski. All rights reserved.
//

import Foundation
import UIKit

class MainMenuViewController: UIViewController {
    @IBOutlet weak var head2headOption: UIButton!
    @IBAction func unwindToMenuViewController(_ segue: UIStoryboardSegue) {
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let sender2 = sender as? UIButton {
            if sender2 === head2headOption {
                (segue.destination as? TwoPlayerGameViewController)?.head2headOption = true
            }
        }
    }

}
