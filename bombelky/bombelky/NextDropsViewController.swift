//
//  NextDropsViewController.swift
//  bombelky
//
//  Created by Grzegorz Moscichowski on 07/06/16.
//  Copyright © 2016 Grzegorz Moscichowski. All rights reserved.
//

import Foundation
import UIKit

class NextDropsViewController:UIViewController,GameStatisticsDisplay {
    @IBOutlet weak var nextDropsContainer: GameBoardDisplayView!
    var dropsLayerManager: GameBoardDisplayLayerManagerImp?
    var nextDrops = [Drop]()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let dlm = GameBoardDisplayLayerManagerImp(layer:nextDropsContainer.layer)
        dlm.boardSize = IntVector(1,4)
        dropsLayerManager = dlm
        nextDropsContainer.layerManager = dlm
        nextDropsContainer.setNeedsLayout()
    }
    func addDrop(_ drop: Drop) {
        dropsLayerManager?.addDrop(drop)
    }
    func removeDropByTag(_ tag: String?) {
        dropsLayerManager?.removeDropByTag(tag)
    }
}
