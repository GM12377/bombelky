//
//  BoardDropsArchivingTests.swift
//  bombelky
//
//  Created by Grzegorz Moscichowski on 17/05/16.
//  Copyright © 2016 Grzegorz Moscichowski. All rights reserved.
//

import XCTest

class BoardDropsArchivingTests: XCTestCase {
    var drops:BoardDropsData?
    override func setUp() {
        super.setUp()
        drops = BoardDropsData(width:3)
    }

    func testInitFromString() {
        let input = "... ... ..A"
        drops!.loadDrops(input, dropStore: FakeDropStore())
        let drop = drops!.getDrop(IntVector(2,0))
        XCTAssert(drop!.color == .colorA)
    }
    func testInitFromStringAndBack() {
        let input = "... .B. ab."
        drops!.loadDrops(input, dropStore: FakeDropStore())
        XCTAssert(drops!.getDrop(IntVector(0,0))!.color == .colorA)
        XCTAssert(drops!.getDrop(IntVector(1,1))!.color == .colorB)
        XCTAssertEqual(drops!.generateDescription(), input)
    }
    func testInitFromStringAndBack2() {
        let input = "... .#. .#. #b. aB#"
        drops!.loadDrops(input, dropStore: FakeDropStore())
        XCTAssert(drops!.getDrop(IntVector(0,0))!.color == .colorA)
        XCTAssert(drops!.getDrop(IntVector(1,1))!.color == .colorB)
        XCTAssertEqual(drops!.generateDescription(), input)
    }
    func testReturnSetOfAddedDrops() {
        let input = "... .#. .#. #B. AB#"
        let dropsAdded = drops!.loadDrops(input, dropStore: FakeDropStore())
        XCTAssertEqual(dropsAdded.count, 7)
    }
    
    func testInitUniqueTags() {
        let input = "... .B. AB."
        drops!.loadDrops(input, dropStore: FakeDropStore())
        XCTAssert(drops!.getDrop(IntVector(0,0))!.tag != drops!.getDrop(IntVector(1,1))!.tag)
    }

}
