//
//  GameBoard.swift
//  bombelky
//
//  Created by Grzegorz Moscichowski on 15/05/16.
//  Copyright © 2016 Grzegorz Moscichowski. All rights reserved.
//

import Foundation
import UIKit

protocol GameController:class {
    func startGame()
    func insertTrash(_ trash:Int)
    weak var opponentGameController:GameController? {get set}
}

protocol GameSteering {
    func fall()
    func move(_ normalizedVector:CGPoint)
    func rotate(_ degrees:Int)
    func fingerUp()
}

enum GameState {
    case readyToStart
    case started
    case gameOver
}

protocol GameControllerDelegate:class {
    func gameController(_ gameController:GameController, stateChanged state:GameState)
}

class GameControllerImp:GameController,GameSteering {
    var display:GameBoardDisplay // TODO: forced to be var after migration to swift 4 - check if it is correct
    let statisticsDisplay:GameStatisticsDisplay
    let options:GameOptions
    var activeDrops:ActiveDropsContainer
    let boardDrops:BoardDropsContainer
    let nextDrops:NextDrops
    var gameState = GameState.readyToStart {
        didSet(previousState) {
            delegate?.gameController(self, stateChanged: gameState)
        }
    }
    var moveAcumulator = CGPoint.zero
    weak var delegate:GameControllerDelegate?
    var cumulatedTrash = 0
    
    // MARK: init
    convenience init(display:GameBoardDisplay, statisticsDisplay:GameStatisticsDisplay, options:GameOptions) {
        let dropStore = DropStore(colorGenerator: RandomColorGenerator(options: options))
        let boardDrops = BoardDropsContainer(display: display, options: options, dropStore: dropStore)
        
        let startCoords = ActiveDropsContainer.calculateStartCoords(options)
        let collisionDetector = BoardCollisionDetector(dropsContainer:boardDrops)
        let activeDrops = ActiveDropsContainer(display: display, startCoords: startCoords, dropStore: dropStore, collisionDetector: collisionDetector)
        
        let nextDrops = NextDrops(dropStore: dropStore, display: statisticsDisplay)
        self.init(display:display, statisticsDisplay:statisticsDisplay, options: options, activeDrops: activeDrops, boardDrops: boardDrops, nextDrops: nextDrops)
    }
    init(display:GameBoardDisplay, statisticsDisplay:GameStatisticsDisplay, options:GameOptions, activeDrops:ActiveDropsContainer, boardDrops:BoardDropsContainer, nextDrops:NextDrops) {
        self.display = display
        self.display.boardSize = options.boardSize
        self.options = options
        self.activeDrops = activeDrops
        self.boardDrops = boardDrops
        self.statisticsDisplay = statisticsDisplay
        self.nextDrops = nextDrops
    }

    // MARK: GameController
    weak var opponentGameController: GameController?
    func startGame() {
        gameState = .started
        boardDrops.removeAllDrops()
        activeDrops.removeAllDrops()
        _ = activeDrops.addDrops()
        nextDrops.updateNextDrops()
    }
    func insertTrash(_ trash:Int) {
        cumulatedTrash += trash
    }

    // MARK: GameSteering
    func fall() {
        if gameState != .started {
            return
        }
        activeDrops.fallDropsToContainer(boardDrops)
        _blowBlobs()
        boardDrops.spillDrops()
        boardDrops.addTrash(cumulatedTrash)
        cumulatedTrash = 0
        if !activeDrops.addDrops() {
            gameState = .gameOver
        }
        nextDrops.updateNextDrops()
    }
    fileprivate func _blowBlobs() {
        var trashToSend = 0
        var bonus:Bonus
        repeat {
            bonus = boardDrops.blowBlobs()
            trashToSend += bonus.trashToSend
        } while bonus.dropsBlown > 0
        opponentGameController?.insertTrash(trashToSend)
    }
    func move(_ normalizedVector: CGPoint) {
        if gameState != .started {
            return
        }
        var scaledVector = CGPoint(x:normalizedVector.x * CGFloat(options.boardSize.x), y: normalizedVector.y * CGFloat(options.boardSize.y))
        if scaledVector.y > 0 {
            scaledVector.y = 0
        }
        moveAcumulator = activeDrops.moveByVector(moveAcumulator + scaledVector)
        if (moveAcumulator.y < 0) {
            moveAcumulator = CGPoint.zero
            fall()
        }
    }
    func rotate(_ degrees: Int) {
        if gameState != .started {
            return
        }
        activeDrops.rotateRightByAngle(degrees)
    }
    func fingerUp() {
        moveAcumulator.x = 0
        _ = activeDrops.moveByVector(moveAcumulator)
        print("fingerUp")
    }
}
