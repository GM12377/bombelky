//
//  ObservingView.swift
//  bombelky
//
//  Created by Grzegorz Moscichowski on 02/06/16.
//  Copyright © 2016 Grzegorz Moscichowski. All rights reserved.
//

import Foundation
import UIKit

class GameBoardDisplayView: UIView {
    var layerManager:GameBoardDisplayLayerManager?
    override func layoutSubviews() {
        super.layoutSubviews()
        layerManager?.layer = layer
        layerManager?.layerBoundsChanged(bounds)
    }
}
