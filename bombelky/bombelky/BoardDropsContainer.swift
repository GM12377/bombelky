//
//  BoardDropsContainer.swift
//  bombelky
//
//  Created by Grzegorz Moscichowski on 13/05/16.
//  Copyright © 2016 Grzegorz Moscichowski. All rights reserved.
//

import Foundation

class BoardDropsContainer:DropsContainer,BoardDropsContainerActions {
    var display:GameBoardDisplay?
    var dropStore:DropStore
    var data : BoardDropsData
    
    init(width:Int, dropStore:DropStore) {
        data = BoardDropsData(width: width)
        self.dropStore = dropStore
    }
    convenience init(display:GameBoardDisplay, options:GameOptionsBoardSize, dropStore:DropStore) {
        self.init(width:options.boardSize.x, dropStore:dropStore)
        self.display = display
    }
    func fallExistingDrop(_ drop:Drop, intoColumn column:Int) {
        if let drop = data.addDrop(drop, onTopOfColumn: column) {
            self.display?.moveDrop(drop.tag, to: drop.position)
        }
    }
    func fallFromSkyDrop(_ drop:Drop, intoColumn column:Int) {
        if let drop = data.addDrop(drop, onTopOfColumn: column) {
            self.display?.addDrop(drop)
        }
    }
    
    func isEmptyLocation(_ location:IntVector) -> Bool {
        return data.isEmptyLocation(location)
    }
    func removeDropOnPosition(_ location:IntVector) {
        data.removeDropOnPosition(location)
    }
    func spillDrops() {
        data.spillDrops{(drop: Drop)->Void in
            self.display?.refreshSpillForDrop(drop)
        }
    }
    @discardableResult func blowBlobs() -> Bonus {
        let blobs = data.findFragileBlobs()
        let bonus = Bonus(blobs: blobs)
        let dropsToBlow = blobs.allFragileDrops()
        data.removeDrops(dropsToBlow)
        for drop in dropsToBlow {
            self.display?.removeDropByTag(drop.tag)
        }
        let movedDrops = data.fallDrops()
        for drop in movedDrops {
            self.display?.moveDrop(drop.tag, to: drop.position)
        }
        return bonus
    }
    func removeAllDrops() {
        let removedDrops = data.removeAllDrops()
        for drop in removedDrops {
            self.display?.removeDropByTag(drop.tag)
        }
    }
    func addTrash(_ quantity: Int) {
        for _ in 0..<quantity {
            fallFromSkyDrop(dropStore.getTrash(), intoColumn: Int(arc4random()) % data.width)
        }
    }
    //MARK archiving
    func unarchiveBoardState(_ description:String) {
        let dropsAdded = data.loadDrops(description, dropStore:dropStore)
        for drop in dropsAdded {
            self.display?.addDrop(drop)
        }
    }
    func archiveBoardState() -> String {
        return data.generateDescription()
    }
}
