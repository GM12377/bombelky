//
//  BlobsSetTests.swift
//  bombelky
//
//  Created by Grzegorz Moscichowski on 18/05/16.
//  Copyright © 2016 Grzegorz Moscichowski. All rights reserved.
//

import XCTest

class BlobsDataTests: XCTestCase {

    func testBlobSetSearchEmpty() {
        let blobs = BlobsDataImp()
        XCTAssertEqual(blobs.blobIndexWith("2"), nil)
        XCTAssertEqual(blobs.blobIndexWith(nil), nil)
    }
    func testAddDropsToBlobSet() {
        var blobs = BlobsDataImp()
        let drop1 = Drop(color: .colorA, star: false, tag: "1")
        let drop2 = Drop(color: .colorA, star: false, tag: "3")
        blobs.addPotentialDropsPair(drop1, neighbour: drop2)
        XCTAssertEqual(blobs.blobIndexWith("1"), 0)
        XCTAssertEqual(blobs.blobIndexWith("3"), 0)
    }
    func testAddDropsToBlobSetWithMissingTags() {
        var blobs = BlobsDataImp()
        let drop1 = Drop(color: .colorA, star: false, tag: "1")
        let drop2 = Drop(color: .colorA)
        blobs.addPotentialDropsPair(drop1, neighbour: drop2)
        XCTAssertEqual(blobs.blobIndexWith("1"), nil)
        XCTAssertEqual(blobs.blobIndexWith("3"), nil)
    }
    func testAddDropsToBlobSetWithMissingTags2() {
        var blobs = BlobsDataImp()
        let drop1 = Drop(color: .colorA, star: false, tag: "1")
        let drop2 = Drop(color: .colorA)
        blobs.addPotentialDropsPair(drop1, neighbour: drop2)
        XCTAssertEqual(blobs.blobIndexWith("1"), nil)
    }
    func testAddTrashToBlobSet() {
        var blobs = BlobsDataImp()
        let drop1 = Drop(color: .trash, star: false, tag: "1")
        let drop2 = Drop(color: .trash, star: false, tag: "3")
        blobs.addPotentialDropsPair(drop1, neighbour: drop2)
        XCTAssertEqual(blobs.blobIndexWith("1"), nil)
        XCTAssertEqual(blobs.blobIndexWith("3"), nil)
    }

}
