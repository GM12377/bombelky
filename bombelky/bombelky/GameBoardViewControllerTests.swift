//
//  GameBoardViewControllerTests.swift
//  bombelky
//
//  Created by Grzegorz Moscichowski on 28/05/16.
//  Copyright © 2016 Grzegorz Moscichowski. All rights reserved.
//

import XCTest

class GameBoardViewControllerTests: XCTestCase,GameControllerDelegate {
    var expectation:XCTestExpectation?

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func gameController(_ gameController: GameController, stateChanged state: GameState) {
        if state == .gameOver {
            expectation?.fulfill()
        }
    }
    func testGameStateChangeCalledTwiceProblem() {
        let gameViewController = GameBoardViewController()
        _ = gameViewController.view
        let options = GameOptionsFactory.test4_4game()
        let gameController = GameControllerImp(display: FakeGameBoardDisplay(), statisticsDisplay: FakeStatisticsDisplay(), options:options)
        expectation = expectation(description:"stateChange")
        gameController.delegate = self
        gameViewController.gameController = gameController
        gameController.startGame()
        gameController.fall()
        gameController.fall()
        gameController.fall()
        XCTAssert(gameController.gameState == .gameOver)
        gameController.fall()
        waitForExpectations(timeout: 1, handler: nil)
    }


}
