//
//  BonusTests.swift
//  bombelky
//
//  Created by Grzegorz Moscichowski on 07/06/16.
//  Copyright © 2016 Grzegorz Moscichowski. All rights reserved.
//

import XCTest

class BonusTests: XCTestCase {

    func testExampleBonusCalculation() {
        let drops = [Drop(), Drop(), Drop()]
        let blob = Blob(drops: drops, starCount: 2)
        let blobs = [blob, blob]
        let blobsData = BlobsDataImp(blobs:blobs, fragileTrash: drops)
        let bonus = Bonus(blobs: blobsData)
        XCTAssertEqual(bonus.starsBlown, 4)
        XCTAssertEqual(bonus.dropsBlown, 6)
        XCTAssertEqual(bonus.trashBlown, 3)
        XCTAssertEqual(bonus.points, 120)
        XCTAssertEqual(bonus.trashToSend, 3)
    }
}
