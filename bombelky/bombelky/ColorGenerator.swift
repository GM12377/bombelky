//
//  ColorGenerator.swift
//  bombelky
//
//  Created by Grzegorz Moscichowski on 13/05/16.
//  Copyright © 2016 Grzegorz Moscichowski. All rights reserved.
//

import Foundation

protocol ColorGenerator {
    func getNextColor() -> DropColors
}