//
//  IntVector.swift
//  Bomble
//
//  Created by Grzegorz Moscichowski on 07/05/16.
//  Copyright © 2016 Grzegorz Moscichowski. All rights reserved.
//

import Foundation
import UIKit

struct IntVector:Equatable, Hashable {
    var x:Int
    var y:Int
    var cgPoint:CGPoint {
        return CGPoint(x: CGFloat(x), y: CGFloat(y))
    }
    func insideSize(_ size: IntVector) -> Bool {
        if x >= size.x {return false}
        if y >= size.y {return false}
        if x < 0 {return false}
        if y < 0 {return false}        
        return true
    }
    
    init() {
        self.init(0, 0)
    }
    
    init(_ x:Int, _ y:Int) {
        self.init(x:x, y:y)
    }

    init(x:Int, y:Int) {
        self.x = x
        self.y = y
    }
    
    init(_ cgfloat:CGPoint) {
        self.init(Int(round(cgfloat.x)), Int(round(cgfloat.y)))
    }
    //MARK: Hashable
    var hashValue : Int {
        get {
            return "\(x),\(y)".hashValue
        }
    }
    func rotatedBySqareAngle(_ angle:Int) -> IntVector {
        return self * IntVector.squareMultiplierFromAngle(angle)
    }
    static func squareMultiplierFromAngle(_ degrees:Int) -> IntVector {
        let normalizedAngle = (360 + (degrees % 360)) % 360;
        switch normalizedAngle {
        case 90:    return IntVector(0,-1)
        case 180:   return IntVector(-1,0)
        case 270:   return IntVector(0,1)
        default:    return IntVector(1,0)
        }
    }
    static func intVectorByTruncIn(_ cgPoint:CGPoint) -> IntVector {
        return IntVector(_truncIn(cgPoint.x), _truncIn(cgPoint.y))
    }
    fileprivate static func _truncIn(_ x:CGFloat) -> Int {
        if x>0 {
            return Int(ceil(x))
        }
        return Int(floor(x))
    }
    static func intVectorByTrunc(_ cgPoint:CGPoint) -> IntVector {
        return IntVector(CGPoint(x: trunc(cgPoint.x), y: trunc(cgPoint.y)))
    }
}

func + (left: IntVector, right: IntVector) -> IntVector {
    return IntVector(x: left.x + right.x, y: left.y + right.y)
}

func - (left: IntVector, right: IntVector) -> IntVector {
    return IntVector(x: left.x - right.x, y: left.y - right.y)
}

func * (left: IntVector, right: IntVector) -> IntVector {
    return IntVector(x: left.x*right.x - left.y*right.y, y: left.x*right.y + left.y*right.x)
}

func * (left: CGPoint, right: IntVector) -> CGPoint {
    let floatRight = right.cgPoint
    return CGPoint(x: left.x*floatRight.x - left.y*floatRight.y, y: left.x*floatRight.y + left.y*floatRight.x)
}

func == (left: IntVector, right: IntVector) -> Bool {
    return (left.x == right.x) && (left.y == right.y)
}

extension CGPoint {
    func rotatedBySqareAngle(_ angle:Int) -> CGPoint {
        return self * IntVector.squareMultiplierFromAngle(angle)
    }
}

func + (left: CGPoint, right: CGPoint) -> CGPoint {
    return CGPoint(x: left.x + right.x, y: left.y + right.y)
}
func - (left: CGPoint, right: CGPoint) -> CGPoint {
    return CGPoint(x: left.x - right.x, y: left.y - right.y)
}

extension CGFloat {
    var doubleValue:      Double  { return Double(self) }
    var degreesToRadians: CGFloat { return CGFloat(doubleValue * .pi / 180) }
    var radiansToDegrees: CGFloat { return CGFloat(doubleValue * 180 / .pi) }
}
