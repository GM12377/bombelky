//
//  PanGestureAnalysisTests.swift
//  bombelky
//
//  Created by Grzegorz Moscichowski on 25/05/16.
//  Copyright © 2016 Grzegorz Moscichowski. All rights reserved.
//

import XCTest

class FakePanGestureRecognizer:UIPanGestureRecognizer {
    var fakeResult = CGPoint.zero
    var fakeState: UIGestureRecognizerState = .began
    var fakeVelocity = CGPoint.zero
    override var state: UIGestureRecognizerState {
        get {
            return fakeState
        }
    }

    override func translation(in view: UIView?) -> CGPoint {
        return fakeResult
    }
    override func velocity(in view: UIView?) -> CGPoint {
        return fakeVelocity
    }
}

class PanGestureAnalysisTests: XCTestCase {
    var analysis = PanGestureAnalysis()
    var fakeRecognizer = FakePanGestureRecognizer()
    override func setUp() {
        super.setUp()
        analysis.containerView = UIView(frame: CGRect(x: 0,y: 0,width: 10,height: 10))
    }
    func testDragLeft() {
        fakeRecognizer.fakeResult = CGPoint(x: -1, y: 0)
        analysis.processEventFrom(fakeRecognizer)
        XCTAssertEqual(analysis.moveVector, CGPoint(x: -0.1, y: 0))
    }

    func testDragDown() {
        fakeRecognizer.fakeResult = CGPoint(x: 0, y: 1)
        analysis.processEventFrom(fakeRecognizer)
        XCTAssertEqual(analysis.moveVector, CGPoint(x: 0, y: -0.1))
    }

    func testDragDownTwice() {
        fakeRecognizer.fakeResult = CGPoint(x: 0, y: 1)
        analysis.processEventFrom(fakeRecognizer)
        fakeRecognizer.fakeState = .changed
        fakeRecognizer.fakeResult = CGPoint(x: 0, y: 2)
        analysis.processEventFrom(fakeRecognizer)
        XCTAssertEqual(analysis.moveVector, CGPoint(x: 0, y: -0.1))
    }
    func testDragDownTwiceStateBegin() {
        fakeRecognizer.fakeResult = CGPoint(x: 0, y: 1)
        analysis.processEventFrom(fakeRecognizer)
        fakeRecognizer.fakeState = .began
        fakeRecognizer.fakeResult = CGPoint(x: 0, y: 3)
        analysis.processEventFrom(fakeRecognizer)
        XCTAssertEqual(analysis.moveVector, CGPoint(x: 0, y: -0.3))
    }
    
    func testSlideDown() {
        analysis.processEventFrom(fakeRecognizer)
        fakeRecognizer.fakeState = .ended
        fakeRecognizer.fakeVelocity = CGPoint(x: 0, y: 11)
        analysis.processEventFrom(fakeRecognizer)
        XCTAssertEqual(analysis.slideDown, true)
        fakeRecognizer.fakeState = .began
        analysis.processEventFrom(fakeRecognizer)
        XCTAssertEqual(analysis.slideDown, false)
    }

}
