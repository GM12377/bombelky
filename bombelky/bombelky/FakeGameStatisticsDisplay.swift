//
//  FakeGameStatisticsDisplay.swift
//  bombelky
//
//  Created by Grzegorz Moscichowski on 08/06/16.
//  Copyright © 2016 Grzegorz Moscichowski. All rights reserved.
//

import Foundation
import UIKit

class FakeStatisticsDisplay: FakeGameBoardDisplay, GameStatisticsDisplay {
    var pointsCounter:Int?
    override var boardSize: IntVector? {
        get {return IntVector(1,4)}
        set {}
    }
    func setPointsCounter(_ points:Int) {
        pointsCounter = points
    }
}
