//
//  GameOptionsTests.swift
//  bombelky
//
//  Created by Grzegorz Moscichowski on 15/05/16.
//  Copyright © 2016 Grzegorz Moscichowski. All rights reserved.
//

import XCTest

class GameOptionsTests: XCTestCase {

    
    func testDefaults() {
        let options = GameOptions()
        XCTAssertEqual(options.boardSize, IntVector(6,12))
    }

}
