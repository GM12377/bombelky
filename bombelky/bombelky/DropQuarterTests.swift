//
//  DropQuarterTests.swift
//  bombelky
//
//  Created by Grzegorz Moscichowski on 23/05/16.
//  Copyright © 2016 Grzegorz Moscichowski. All rights reserved.
//

import XCTest

class DropQuarterTests: XCTestCase {

    func testAllSticked() {
        let quarter = DropQuarter(neighbours: [true,true,true], radius: 1, rotationDegrees: 0, center:CGPoint(x: 0,y: 0))
        let subpaths = quarter.elements
        XCTAssertEqual(subpaths.count, 1)
        let line = subpaths[0] as! BezierLineData
        XCTAssertEqual(line, BezierLineData(startPoint: CGPoint(x: 1, y: -1), endPoint: CGPoint(x: 1, y: 1)))
    }

    func testRadius() {
        let quarter = DropQuarter(neighbours: [true,true,true], radius: 10, rotationDegrees: 0, center:CGPoint(x: 0,y: 0))
        let subpaths = quarter.elements
        let line = subpaths[0] as! BezierLineData
        XCTAssertEqual(line, BezierLineData(startPoint: CGPoint(x: 10, y: -10), endPoint: CGPoint(x: 10, y: 10)))
    }
    func testRotation() {
        let quarter = DropQuarter(neighbours: [true,true,true], radius: 10, rotationDegrees: 90, center:CGPoint(x: 0,y: 0))
        let subpaths = quarter.elements
        let line = subpaths[0] as! BezierLineData
        XCTAssertEqual(line, BezierLineData(startPoint: CGPoint(x: 10, y: 10), endPoint: CGPoint(x: -10, y: 10)))
    }
    func testNoneSticked() {
        let quarter = DropQuarter(neighbours: [false,false,false], radius: 2, rotationDegrees: 0, center:CGPoint(x: 0,y: 0))
        let subpaths = quarter.elements
        XCTAssertEqual(subpaths.count, 1)
        let arc = subpaths[0] as! BezierArcData
        let expected = BezierArcData(centerPoint: CGPoint(x: 0,y: 0),
                                     radius: 2 * quarter.dropRadius,
                                     startAngle: -45,
                                     endAngle: +45,
                                     clockwise: true)
        XCTAssertEqual(arc, expected)
    }
    func testNoneStickedRotated() {
        let quarter = DropQuarter(neighbours: [false,false,false], radius: 2, rotationDegrees: 90, center:CGPoint(x: 0,y: 0))
        let arc = quarter.elements[0] as! BezierArcData
        let expected = BezierArcData(centerPoint: CGPoint(x: 0,y: 0),
                                     radius: 2 * quarter.dropRadius,
                                     startAngle: 45,
                                     endAngle: 45+90,
                                     clockwise: true)
        XCTAssertEqual(arc, expected)
    }
    func testNoneStickedCustomCenter() {
        let quarter = DropQuarter(neighbours: [false,false,false], radius: 2, rotationDegrees: 0, center:CGPoint(x: 12,y: 21))
        let subpaths = quarter.elements
        XCTAssertEqual(subpaths.count, 1)
        let arc = subpaths[0] as! BezierArcData
        let expected = BezierArcData(centerPoint: CGPoint(x: 12,y: 21),
                                     radius: 2 * quarter.dropRadius,
                                     startAngle: -45,
                                     endAngle: +45,
                                     clockwise: true)
        XCTAssertEqual(arc, expected)
    }
    func testMiddleSticked() {
        let quarter = DropQuarter(neighbours: [false,true,false], radius: 1, rotationDegrees: 0, center:CGPoint(x: 0,y: 0))
        let subpaths = quarter.elements
        XCTAssertEqual(subpaths.count, 3)
        let line = subpaths[1] as! BezierLineData
        XCTAssertEqual(line.endPoint.y - line.startPoint.y, 2*(1+quarter.dropRadius-sqrt(2)), accuracy: 0.001)
        var arc = subpaths[0] as! BezierArcData
        var expected = BezierArcData(centerPoint: CGPoint(x: 1,y: -1),
                                     radius: sqrt(2)-1 * quarter.dropRadius,
                                     startAngle: 90+45,
                                     endAngle: 90,
                                     clockwise: false)
        XCTAssertEqual(arc, expected)
        arc = subpaths[2] as! BezierArcData
        expected = BezierArcData(centerPoint: CGPoint(x: 1,y: 1),
                                     radius: sqrt(2)-1 * quarter.dropRadius,
                                     startAngle: 270,
                                     endAngle: 270-45,
                                     clockwise: false)
        XCTAssertEqual(arc, expected)
    }
    func testMiddleStickedRotated() {
        let quarter = DropQuarter(neighbours: [false,true,false], radius: 1, rotationDegrees: 90, center:CGPoint(x: 0,y: 0))
        let subpaths = quarter.elements
        let line = subpaths[1] as! BezierLineData
        XCTAssertEqual(-line.endPoint.x + line.startPoint.x, 2*(1 + quarter.dropRadius-sqrt(2)), accuracy: 0.001)
        var arc = subpaths[0] as! BezierArcData
        var expected = BezierArcData(centerPoint: CGPoint(x: 1,y: 1),
                                     radius: sqrt(2)-1 * quarter.dropRadius,
                                     startAngle: 180+45,
                                     endAngle: 180,
                                     clockwise: false)
        XCTAssertEqual(arc, expected)
        arc = subpaths[2] as! BezierArcData
        expected = BezierArcData(centerPoint: CGPoint(x: -1,y: 1),
                                 radius: sqrt(2)-1 * quarter.dropRadius,
                                 startAngle: 360,
                                 endAngle: 360-45,
                                 clockwise: false)
        XCTAssertEqual(arc, expected)
    }

    func testDraw() {
        let dropShape = DropPathData(neighbours: Set([.east,.se,.west,.north,.ne]),
                                     radius: 10,
                                     center:CGPoint(x: 10,y: 10))
        let path = dropShape.generatePath()
        XCTAssert(path.contains(CGPoint(x:20, y:0))) /// corners
        XCTAssert(!path.contains(CGPoint(x:0, y:0)))
        XCTAssert(!path.contains(CGPoint(x:20, y:20)))
        XCTAssert(!path.contains(CGPoint(x:0, y:20)))
        XCTAssert(path.contains(CGPoint(x:10, y:10))) /// middle
        XCTAssert(!path.contains(CGPoint(x:10, y:19.99))) /// bottom arc
        XCTAssert(path.contains(CGPoint(x:10, y:0.01))) /// top
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            for _ in 1...60*12*6 {
                let dropShape = DropPathData(neighbours: Set([.east,.se,.west,.north,.ne]),
                    radius: 10,
                    center:CGPoint(x: 10,y: 10))
                let path = dropShape.generatePath()
                XCTAssert(path.contains(CGPoint(x:10, y:10))) /// middle
            }
        }
    }

}
