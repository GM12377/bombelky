//
//  MultiplayerGameViewController.swift
//  bombelky
//
//  Created by Grzegorz Moscichowski on 07/06/16.
//  Copyright © 2016 Grzegorz Moscichowski. All rights reserved.
//

import Foundation
import UIKit

class MultiplayerGameViewController: UIViewController, GameControllerDelegate {
    @IBOutlet var gameBoardContainers: [UIView]!
    var gameViewControllers=[GameBoardViewController]()
    
    override func viewDidLoad() {
        for container in gameBoardContainers {
            let gbvc = GameBoardViewController()
            addChildViewController(gbvc)
            container.addSubview(gbvc.view)
            container.addAllEdgesConstraintsForSubView(gbvc.view)
            gbvc.delegate = self
            gameViewControllers.append(gbvc)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        gameViewControllers.forEach{ $0.startGame() }
    }
    
    override var supportedInterfaceOrientations : UIInterfaceOrientationMask {
        return  .landscape
    }
    @IBAction func unwindToResumeViewController(_ segue: UIStoryboardSegue) {
    }
    @IBAction func unwindToPlayAgainViewController(_ segue: UIStoryboardSegue) {
        gameViewControllers.forEach{ $0.startGame() }
    }
    
    //MARK: GameControllerDelegate
    func gameController(_ gameController: GameController, stateChanged state: GameState) {
        switch state {
        case .gameOver:
            performSegue(withIdentifier: "InGameMenu", sender: self)
        default:
            break
        }
    }
}
