//
//  GameBoardViewController.swift
//  bombelky
//
//  Created by Grzegorz Moscichowski on 20/05/16.
//  Copyright © 2016 Grzegorz Moscichowski. All rights reserved.
//

import Foundation
import UIKit

class GameBoardViewController : UIViewController, GameControllerDelegate {
    var options = GameOptionsFactory.test6_12game()
    var gameController:GameController?
    var gameSteering:GameSteering?
    @IBOutlet weak var dropsContainerView: GameBoardDisplayView!
    var panAnalysis = PanGestureAnalysis()
    weak var delegate:GameControllerDelegate?
    var dropsLayerManager: GameBoardDisplayLayerManagerImp?
    
    @IBOutlet weak var gameStatisticsContainer: UIView!
    var gameStatisticsController:NextDropsViewController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupGameBoard()
    }
    
    func setupGameBoard() {
        let dlm = GameBoardDisplayLayerManagerImp(layer:dropsContainerView.layer)
        dlm.boardSize = options.boardSize
        dropsLayerManager = dlm
        dropsContainerView.layerManager = dlm
        dropsContainerView.setNeedsLayout()
        setupNextDropsPanel()
        let gc = GameControllerImp(display: dlm, statisticsDisplay: gameStatisticsController!, options:options)
        gc.delegate = self
        gameController = gc
        gameSteering = gc
        panAnalysis.containerView = dropsContainerView
    }
    func setupNextDropsPanel() {
        let gsvc = NextDropsViewController()
        gameStatisticsController = gsvc
        addChildViewController(gsvc)
        gameStatisticsContainer.addSubview(gsvc.view)
        gameStatisticsContainer.addAllEdgesConstraintsForSubView(gsvc.view)
    }
    
    func startGame() {
        gameController!.startGame()
    }
    @IBAction func rotateLeft(_ sender: UITapGestureRecognizer) {
        gameSteering?.rotate(-90)
    }
    @IBAction func rotateRight(_ sender: UITapGestureRecognizer) {
        gameSteering?.rotate(90)
    }
    @IBAction func drag(_ sender: UIPanGestureRecognizer) {
        panAnalysis.processEventFrom(sender)
        if panAnalysis.slideDown {
            gameSteering?.fall()
            return
        }
        if panAnalysis.fingerUp {
            gameSteering?.fingerUp()
        }
        gameSteering?.move(panAnalysis.moveVector)
    }
    
    //MARK: GameControllerDelegate
    func gameController(_ gameController: GameController, stateChanged state: GameState) {
        delegate?.gameController(gameController, stateChanged: state)
    }
}
