//
//  CollisionDetector.swift
//  bombelky
//
//  Created by Grzegorz Moscichowski on 13/05/16.
//  Copyright © 2016 Grzegorz Moscichowski. All rights reserved.
//

import Foundation
import UIKit

protocol CollisionDetector {
    func cropPathFrom(_ position:IntVector, vector:IntVector) -> IntVector
    func cropPathFrom(_ positions:[IntVector], vector:CGPoint) -> CGPoint
}
