//
//  DropGenerator.swift
//  bombelky
//
//  Created by Grzegorz Moscichowski on 13/05/16.
//  Copyright © 2016 Grzegorz Moscichowski. All rights reserved.
//

import Foundation

struct TagGenerator {
    fileprivate var lastTag = 0
    mutating func getNextTag() -> String {
        lastTag += 1
        return lastTag.description
    }
}

/// generates new drops, assigns colors, assigns start column for trash
class DropStore {
    var colorGenerator:ColorGenerator
    var tagGenerator = TagGenerator()
    fileprivate var nextDrops = [Drop]()
    init(colorGenerator:ColorGenerator) {
        self.colorGenerator = colorGenerator
        _rollDropPair()
    }
    fileprivate func _rollDropPair() {
        nextDrops.removeAll()
        var drop = generateActiveDrop()
        drop.star = true
        nextDrops.append(drop)
        nextDrops.append(generateActiveDrop())
    }
//    private func _rollBuffer() {
//        if nextDrops.count == 2 {
//            nextDrops[0] = nextDrops[1]
//            nextDrops.removeLast()
//        }
//        nextDrops.append(generateActiveDrop())
//    }
    fileprivate func generateActiveDrop() -> Drop {
        var drop = Drop(color: colorGenerator.getNextColor())
        drop.tag = tagGenerator.getNextTag()
        return drop
    }
    func getActiveDrops() -> (first:Drop, second:Drop) {
        var drop1 = nextDrops[0]
        drop1.star = true
        let drop2 = nextDrops[1]
        _rollDropPair()
        return (drop1, drop2)
    }
    func getTrash() -> Drop {
        var drop = Drop(color: .trash)
        drop.tag = tagGenerator.getNextTag()
        return drop
    }
    func getNextDrop(_ step:Int) -> Drop? {
        if nextDrops.count > step {
            return nextDrops[step]
        }
        return nil
    }
}
