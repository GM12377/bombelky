//
//  RandomizeTests.swift
//  bombelky
//
//  Created by Grzegorz Moscichowski on 15/05/16.
//  Copyright © 2016 Grzegorz Moscichowski. All rights reserved.
//

import XCTest

class RandomizeTests: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testRandomSeqence() {
        var rand = Randomizer(seed: 7)
        var result = [Int]()
        for _ in 0...9 {
            result.append(rand.rand(10))
        }
        XCTAssertEqual(result, [7,9,9,1,5,3,6,7,0,3])
    }
    func testCopyingRandomizerState() {
        var randomizer1 = Randomizer(seed: 7)
        for _ in 0...12 {
            _ = randomizer1.rand()
        }
        var randomizer2 = randomizer1
        var result1 = [Int]()
        var result2 = [Int]()
        for _ in 0...9 {
            result1.append(randomizer1.rand())
            result2.append(randomizer2.rand())
        }
        XCTAssertEqual(result1, result2)
    }
    
    /// MARK: alternative (looks it is removed in swift 3)
//    func testrand_r() {
//        var result1 = [Int32]()
//        var result2 = [Int32]()
//        var seed1: UInt32 = 7
//        var seed2: UInt32 = 7
//        for _ in 0...9 {
//            result1.append(rand_r(&seed1))
//            result2.append(rand_r(&seed2))
//        }
//        XCTAssertEqual(result1, result2)
//    }

}
