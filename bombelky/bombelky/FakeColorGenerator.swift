//
//  FakeColorGenerator.swift
//  bombelky
//
//  Created by Grzegorz Moscichowski on 13/05/16.
//  Copyright © 2016 Grzegorz Moscichowski. All rights reserved.
//

import Foundation

class FakeColorGenerator: ColorGenerator {
    var nextColors=[DropColors]()
    func getNextColor() -> DropColors {
        if let color = nextColors.first {
            nextColors.removeFirst()
            return color
        }
        return .trash
    }
}
