//
//  GameBoardDisplay.swift
//  Bomble
//
//  Created by Grzegorz Moscichowski on 13/05/16.
//  Copyright © 2016 Grzegorz Moscichowski. All rights reserved.
//

import Foundation
import UIKit

///
protocol GameBoardDisplay {
    var boardSize : IntVector? {get set}
    func addDrop(_ drop:Drop)
    func removeDropByTag(_ tag: String?)
    func moveDrop(_ tag:String?, to:IntVector)
    func moveDrop(_ tag:String?, to:CGPoint)
    func refreshSpillForDrop(_ drop: Drop)
}
