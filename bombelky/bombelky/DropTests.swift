//
//  DropTests.swift
//  Bomble
//
//  Created by Grzegorz Moscichowski on 12/05/16.
//  Copyright © 2016 Grzegorz Moscichowski. All rights reserved.
//

import XCTest

class DropTests: XCTestCase {
    
    func testExampleSwift() {
        var drop = Drop()
        XCTAssertEqual(drop.position, IntVector())
        drop.position = IntVector(x:4, y:6)
        XCTAssertEqual(drop.position, IntVector(x:4, y:6))
    }
    
    func testInitWithPosition() {
        let pos = IntVector(x: 3, y: 3)
        let drop = Drop(position:pos)
        XCTAssertEqual(drop.position, pos)
    }
    
    func testDropComparison()  {
        let drop1 = Drop(color: .colorA, position: IntVector(1,2), star:false, tag:"t")
        let drop2 = Drop(color: .colorA, position: IntVector(1,2), star:false, tag:"t")
        XCTAssertEqual(drop1, drop2)
    }
    
}
