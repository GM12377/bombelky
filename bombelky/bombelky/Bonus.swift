//
//  BonusCalculator.swift
//  bombelky
//
//  Created by Grzegorz Moscichowski on 07/06/16.
//  Copyright © 2016 Grzegorz Moscichowski. All rights reserved.
//

import Foundation

struct Bonus {
    var points = 0
    var trashToSend = 0
    var dropsBlown = 0
    var starsBlown = 0
    var trashBlown = 0
    
    static let pointsPerDrop = 10
    static let pointsPerTrash = 0
    static let pointsPerStar = 10
    
    init(blobs:BlobsData) {
        dropsBlown = blobs.blobs.reduce(0) {$0 + $1.drops.count}
        starsBlown = blobs.blobs.reduce(0) {$0 + $1.starCount}
        trashBlown = blobs.fragileTrash.count
        let pointsForDrops = blobs.blobs.reduce(0) {$0 + Bonus.pointsForPool($1)}
        points = pointsForDrops + trashBlown * Bonus.pointsPerTrash
        let trashCalculation = blobs.blobs.reduce(0.0) {$0 + Double($1.drops.count - 2) * 1.5}
        trashToSend = Int( trashCalculation )
    }
    static func pointsForPool(_ blob:Blob) -> Int {
        let dropsCount = blob.drops.count
        return  blob.starCount * pointsPerStar + (dropsCount - 1) * (dropsCount - 1) * pointsPerDrop
    }
}
