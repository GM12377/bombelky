//
//  FakeBoardDropsContainer.swift
//  bombelky
//
//  Created by Grzegorz Moscichowski on 24/05/16.
//  Copyright © 2016 Grzegorz Moscichowski. All rights reserved.
//

import Foundation

class FakeBoardDropsContainer:BoardDropsContainer {
    var fakeDisplay : FakeGameBoardDisplay {
        return display! as! FakeGameBoardDisplay
    }
    
    init() {
        super.init(width:3, dropStore:FakeDropStore())
        display = FakeGameBoardDisplay()
    }
    func fallTo20() {
        fallTo(2)
    }
    func fallTo(_ column:Int) {
        fallFromSkyDrop(dropStore.getActiveDrops().first, intoColumn:column)
    }
}
