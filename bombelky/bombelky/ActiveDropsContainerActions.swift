
import Foundation
import UIKit

protocol ActiveDropsContainerActions {
    func addDrops() -> Bool
    func fallDropsToContainer(_ container: BoardDropsContainer)
    func moveByVector(_ vector: IntVector) -> IntVector
    func moveByVector(_ vector: CGPoint) -> CGPoint
    func rotateRightByAngle(_ degrees:Int)
}
