//
//  DropStoreTests.swift
//  bombelky
//
//  Created by Grzegorz Moscichowski on 13/05/16.
//  Copyright © 2016 Grzegorz Moscichowski. All rights reserved.
//

import XCTest

class DropStoreTests: XCTestCase {

    func testTagGeneration() {
        let store = DropStore(colorGenerator:FakeColorGenerator())
        let activeDrops = store.getActiveDrops()
        XCTAssertEqual(activeDrops.first.tag, "1")
        XCTAssertEqual(activeDrops.second.tag, "2")
    }
    func testColorGeneration() {
        let colorGen = FakeColorGenerator()
        colorGen.nextColors = [.colorB, .colorA, .colorC, .colorD]
        let store = DropStore(colorGenerator:colorGen)
        let activeDrops = store.getActiveDrops()
        XCTAssert(activeDrops.first.color == .colorB)
        XCTAssert(activeDrops.second.color == .colorA)
    }
    func testTrashGeneration() {
        let colorGen = FakeColorGenerator()
        let store = DropStore(colorGenerator:colorGen)
        XCTAssert(store.getTrash().color == .trash)
    }
    // MARK: next drops
    func testNextDrops() {
        let colorGen = RandomColorGenerator(seed:7, lastColor: .colorE)
        let store = DropStore(colorGenerator:colorGen)
        let nextDrop1 = store.getNextDrop(0)
        let nextDrop2 = store.getNextDrop(1)
        let activeDrops = store.getActiveDrops()
        XCTAssertEqual(nextDrop1, activeDrops.first)
        XCTAssertEqual(nextDrop2, activeDrops.second)
    }
    
}
