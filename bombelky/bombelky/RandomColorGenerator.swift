//
//  RandomColorGenerator.swift
//  bombelky
//
//  Created by Grzegorz Moscichowski on 16/05/16.
//  Copyright © 2016 Grzegorz Moscichowski. All rights reserved.
//

import Foundation

class RandomColorGenerator: ColorGenerator {
    var randomizer:Randomizer
    var lastColor:DropColors
    
    init(seed:Int, lastColor:DropColors)  {
        randomizer = Randomizer(seed: seed)
        self.lastColor = lastColor
    }
    convenience init(options:GameOptionsDropRandomization) {
        self.init(seed:options.randomColorsSeed, lastColor: options.lastDropColor)
    }

    func getNextColor() -> DropColors {
        return DropColors(rawValue: randomizer.rand(lastColor.rawValue + 1))!
    }

}