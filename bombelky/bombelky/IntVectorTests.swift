//
//  IntVectorTests.swift
//  Bomble
//
//  Created by Grzegorz Moscichowski on 07/05/16.
//  Copyright © 2016 Grzegorz Moscichowski. All rights reserved.
//

import XCTest

class IntVectorTests: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testDefaultValue() {
        let coord = IntVector()
        XCTAssertEqual(coord.x, 0)
        XCTAssertEqual(coord.x, 0)
    }

    func testInit() {
        let coord = IntVector(x: 3, y: 4)
        XCTAssertEqual(coord.x, 3)
        XCTAssertEqual(coord.y, 4)
    }

    func testMutate() {
        var coord = IntVector(x: 3, y: 4)
        coord.x = 7
        XCTAssertEqual(coord.x, 7)
        XCTAssertEqual(coord.y, 4)
    }
    
    func testAdd() {
        let coord = IntVector(x: 3, y: 4)
        let vector = IntVector(x: 2, y: -2)
        let result = coord + vector
        XCTAssertEqual(result.x, 5)
        XCTAssertEqual(result.y, 2)
    }

    func testSub() {
        let coord = IntVector(x: 3, y: 4)
        let vector = IntVector(x: 2, y: -2)
        let result = coord - vector
        XCTAssertEqual(result.x, 1)
        XCTAssertEqual(result.y, 6)
    }
    
    func testMul() {
        let coord = IntVector(x: 3, y: 4)
        let vector = IntVector(x: 0, y: -1)
        let result = coord * vector
        XCTAssertEqual(result.x, 4)
        XCTAssertEqual(result.y, -3)
    }
    
    func testInsideSize() {
        let vector = IntVector(x: 3, y: 4)
        let size = IntVector(x: 4, y: 5)
        XCTAssert(vector.insideSize(size))
    }

    func testInsideSizeY() {
        let vector = IntVector(x: 3, y: 4)
        let size = IntVector(x: 4, y: 4)
        XCTAssert(!vector.insideSize(size))
    }

    func testInsideSizeX() {
        let vector = IntVector(x: 10, y: 4)
        let size = IntVector(x: 4, y: 40)
        XCTAssert(!vector.insideSize(size))
    }
    func testInsideSizeMinus() {
        let size = IntVector(x: 40, y: 40)
        let vector = IntVector(x: -1, y: 4)
        XCTAssert(!vector.insideSize(size))
        let vector2 = IntVector(x: 1, y: -1)
        XCTAssert(!vector2.insideSize(size))
    }
    func testRotate() {
        let vector = IntVector(10,-2)
        let rotated = vector.rotatedBySqareAngle(90)
        XCTAssertEqual(rotated, IntVector(-2,-10))
    }
    func testIntVectorFromCGPoint() {
        let point = CGPoint(x: 0.999, y: -0.999)
        let vector = IntVector(point)
        XCTAssertEqual(vector, IntVector(1,-1))
    }
    func testIntFromCGFloat() {
        let floatNumber:CGFloat = 6.99999998
        let number = Int(round(floatNumber))
        XCTAssertEqual(number, 7)
    }
}
