//
//  CollisionDetector.swift
//  bombelky
//
//  Created by Grzegorz Moscichowski on 13/05/16.
//  Copyright © 2016 Grzegorz Moscichowski. All rights reserved.
//

import Foundation
import UIKit

class BoardCollisionDetector:CollisionDetector {
    static let hysteresis:CGFloat = 0.6
    let dropsContainer : BoardDropsContainer

    init(dropsContainer:BoardDropsContainer) {
        self.dropsContainer = dropsContainer
    }
    func cropPathFrom(_ positions: [IntVector], vector: CGPoint) -> CGPoint {
        var longestVector = vector
        for position in positions {
            longestVector = _cropPathFrom(position, vector: longestVector)
        }
        return longestVector
    }
    fileprivate func _cropPathFrom(_ position:IntVector, vector: CGPoint) -> CGPoint {
        let inputVectorCeil = IntVector.intVectorByTruncIn(vector)
        let longestVector = cropPathFrom(position, vector: inputVectorCeil)
        var result = vector
        if inputVectorCeil.y != longestVector.y {
            result.y = CGFloat(longestVector.y)
        }
        if inputVectorCeil.x != longestVector.x {
            result.x = CGFloat(longestVector.x)
            return result
        }
        
        let columnsToCheck = _columnsFrom(position.x, xVector:result.x)
        let longestSlideColumnVector = cropPathFrom(IntVector(columnsToCheck.slideInColumn,position.y), vector: IntVector(0, Int(floor(vector.y))))
        let longestSecondColumnVector = cropPathFrom(IntVector(columnsToCheck.secondColumn,position.y), vector: IntVector(0, Int(floor(vector.y))))
        
        result = vector
        if longestSlideColumnVector.y < longestSecondColumnVector.y {
            result.x =  CGFloat(columnsToCheck.slideInColumn - position.x)
        }
        if CGFloat(longestSlideColumnVector.y) > result.y {
            result.y = CGFloat(longestSlideColumnVector.y)
        }
        return result
    }
    func _columnsFrom(_ startColumn:Int, xVector:CGFloat) -> (slideInColumn:Int, secondColumn:Int) {
        let truncatedVector = trunc(xVector)
        var vectorSign = 1
        if xVector < 0 {
            vectorSign = -1
        }
        let nearColumn = startColumn + Int(truncatedVector)
        let farColumn = nearColumn + vectorSign
        if (xVector - truncatedVector) * CGFloat(vectorSign) > BoardCollisionDetector.hysteresis {
            return (farColumn, nearColumn)
        }
        return (nearColumn, farColumn)
    }
    
    /// IntVector Collision
    func cropPathFrom(_ position:IntVector, vector:IntVector) -> IntVector {
        var longestVector = _cropXPathFrom(position, vector: vector)
        if (vector.y>0) {
            return IntVector(longestVector.x, vector.y)
        }
        if (vector.y<0) {
            for i in (vector.y...0).reversed() {
                if !dropsContainer.isEmptyLocation(position + IntVector(longestVector.x, i)) {
                    return longestVector
                }
                longestVector.y = i
            }
        }
        return longestVector
    }
    fileprivate func _cropXPathFrom(_ position:IntVector, vector:IntVector) -> IntVector {
        var longestVector = IntVector()
        var step = 1
        if vector.x < 0 {
            step = -1
        }
        for i in stride(from: 0, through: vector.x, by: step) {
            if !dropsContainer.isEmptyLocation(position + IntVector(i, 0)) {
                return longestVector
            }
            longestVector.x = i
        }
        return longestVector
    }
}
