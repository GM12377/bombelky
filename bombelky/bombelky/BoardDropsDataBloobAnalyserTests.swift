//
//  BloobAnalyserTests.swift
//  bombelky
//
//  Created by Grzegorz Moscichowski on 14/05/16.
//  Copyright © 2016 Grzegorz Moscichowski. All rights reserved.
//

import XCTest

class BloobAnalyserTests: XCTestCase {
    var container : FakeBoardDropsContainer = FakeBoardDropsContainer()

    func testSpill() {
        container.fallTo(2)
        container.fallTo(2)
        container.spillDrops()
        XCTAssertEqual(container.fakeDisplay.addedDrops[0].spillDirections, Set([.north]))
        XCTAssertEqual(container.fakeDisplay.addedDrops[1].spillDirections, Set([.south]))
    }
    func testSpillHorizontal() {
        container.fallTo(1)
        container.fallTo(2)
        container.spillDrops()
        XCTAssertEqual(container.fakeDisplay.addedDrops[0].spillDirections, Set([.east]))
        XCTAssertEqual(container.fakeDisplay.addedDrops[1].spillDirections, Set([.west]))
    }
    func testSpillSquare() {
        let input = "... .bb .bb"
        container.unarchiveBoardState(input)
        container.spillDrops()
        XCTAssertEqual(container.fakeDisplay.addedDrops[0].spillDirections, Set([.west, .north, .nw]))
    }
    func testBlobDetection1() {
        let input = "... .B. AB."
        container.unarchiveBoardState(input)
        let dropsData = container.data
        let blobsData = dropsData.findFragileBlobs()
        XCTAssertEqual(blobsData.blobs.count, 1)
        let tag = blobsData.blobs[0].drops.first!.tag
        let dropFromBlob = dropsData.getDrop(tag)
        XCTAssert(dropFromBlob!.color == .colorB)
    }
    func testBlobDetection2() {
        let input = "... ..B AAB"
        container.unarchiveBoardState(input)
        let dropsData = container.data
        let blobData = dropsData.findFragileBlobs()
        XCTAssertEqual(blobData.blobs.count, 2)
    }
    func testBlobDetectionComplex() {
        let input = "Aa. BA. AA. BA."
        container.unarchiveBoardState(input)
        let dropsData = container.data
        let blobsData = dropsData.findFragileBlobs()
        XCTAssertEqual(blobsData.blobs.count, 1)
        XCTAssertEqual( blobsData.blobs[0].drops.count, 6)
    }
    func testFragileTrashDetection() {
        let input = ".#. .#. #B. AB#"
        container.unarchiveBoardState(input)
        let dropsData = container.data
        let blobsData = dropsData.findFragileBlobs()
        XCTAssertEqual(blobsData.fragileTrash.count, 3)
    }
    func testBlobDetectionLessThen2Stars() {
        let input = "... .B. Ab."
        container.unarchiveBoardState(input)
        let blobsData = container.data.findFragileBlobs()
        XCTAssertEqual(blobsData.blobs.count, 0)
    }

    func testBlobDetectionComplex2() {
        let display = FakeGameBoardDisplay()
        let container = BoardDropsContainer(display:display,
                                        options:GameOptions(boardSize: IntVector(6,12)),
                                        dropStore:FakeDropStore())

        container.unarchiveBoardState("...... .B.... .b.... cbcC.. Cccc..")
        let dropsData = container.data
        let blobsData = dropsData.findFragileBlobs()
        XCTAssertEqual(blobsData.blobs.count, 1)
        XCTAssertEqual( blobsData.blobs[0].drops.count, 7)
    }
// square blob have a hole inside - there is no neighbour detection on (1,1) vector
}
