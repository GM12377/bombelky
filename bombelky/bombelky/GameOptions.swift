//
//  GameOptions.swift
//  bombelky
//
//  Created by Grzegorz Moscichowski on 15/05/16.
//  Copyright © 2016 Grzegorz Moscichowski. All rights reserved.
//

import Foundation

protocol GameOptionsBoardSize {
    var boardSize:IntVector {get}
}

protocol GameOptionsDropRandomization {
    var randomColorsSeed:Int {get}
    var lastDropColor:DropColors {get set}
}

struct GameOptions:GameOptionsBoardSize, GameOptionsDropRandomization {
    let boardSize:IntVector
    let randomColorsSeed:Int
    var lastDropColor:DropColors
    
    init() {
        self.init(boardSize: nil, randomColorsSeed: nil, dropColorsNumber:nil)
    }
    init(boardSize:IntVector) {
        self.init(boardSize: boardSize, randomColorsSeed: nil, dropColorsNumber:nil)
    }
    init(randomColorsSeed:Int, dropColorsNumber:Int) {
        self.init(boardSize: nil, randomColorsSeed: randomColorsSeed, dropColorsNumber:dropColorsNumber)
    }
    
    init(boardSize:IntVector?, randomColorsSeed:Int?, dropColorsNumber:Int?) {
        self.boardSize = boardSize ?? IntVector(6,12)
        self.randomColorsSeed = randomColorsSeed ?? Int(arc4random())
        self.lastDropColor = DropColors(rawValue: (dropColorsNumber ?? GameOptions.defaultLastDropColor().rawValue) - 1) ?? GameOptions.defaultLastDropColor()
    }
    static func defaultLastDropColor() -> DropColors {
        return .colorD
    }

}
