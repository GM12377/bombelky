//
//  CollisionDetectorTests.swift
//  bombelky
//
//  Created by Grzegorz Moscichowski on 13/05/16.
//  Copyright © 2016 Grzegorz Moscichowski. All rights reserved.
//

import XCTest

class CollisionDetectorTests: XCTestCase {
    var collisionDetector : CollisionDetector?
    var dropsContainer : BoardDropsContainer?
    
    override func setUp() {
        super.setUp()
        dropsContainer = BoardDropsContainer(width: 4, dropStore: FakeDropStore())
        collisionDetector = BoardCollisionDetector(dropsContainer: dropsContainer!)
    }
    
    func test0Vector() {
        let vector = IntVector()
        let result = collisionDetector!.cropPathFrom(IntVector(3,3), vector: vector)
        XCTAssertEqual(result, vector)
    }
    func testNoCollision() {
        let vector = IntVector(-1, -1)
        let result = collisionDetector!.cropPathFrom(IntVector(2,3), vector: vector)
        XCTAssertEqual(result, vector)
    }
    func testBoundsCollisionS() {
        let result = collisionDetector!.cropPathFrom(IntVector(3,3), vector: IntVector(0,-10))
        XCTAssertEqual(result, IntVector(0,-3))
    }
    func testBoundsCollisionE() {
        let result = collisionDetector!.cropPathFrom(IntVector(0,0), vector: IntVector(10,0))
        XCTAssertEqual(result, IntVector(3,0))
    }
    func testNoCollisionUpDirection() {
        let result = collisionDetector!.cropPathFrom(IntVector(0,0), vector: IntVector(0,1))
        XCTAssertEqual(result, IntVector(0,1))
    }
    // MARK: float collision calculations
    func testNoCollisionFloat() {
        let vector = CGPoint(x: 1, y: 0)
        let positions = [IntVector(1,1), IntVector(1,0)]
        let result = collisionDetector!.cropPathFrom(positions, vector: vector)
        XCTAssertEqual(result, vector)
    }
    func testNoCollisionFloat2() {
        let vector = CGPoint(x: 1.1, y: -0.1)
        let positions = [IntVector(1,2), IntVector(1,1)]
        let result = collisionDetector!.cropPathFrom(positions, vector: vector)
        XCTAssertEqual(result, vector)
    }
    func testCollisionFloatBottom() {
        let vector = CGPoint(x: 1.1, y: -1.1)
        let positions = [IntVector(1,1)]
        let result = collisionDetector!.cropPathFrom(positions, vector: vector)
        XCTAssertEqual(result, CGPoint(x: 1.1, y: -1))
    }
    func testCollisionFloatE() {
        let vector = CGPoint(x: 3.1, y: -1.1)
        let positions = [IntVector(1,1)]
        let result = collisionDetector!.cropPathFrom(positions, vector: vector)
        XCTAssertEqual(result, CGPoint(x: 2, y: -1))
    }
    func testCollisionFloatW() {
        let vector = CGPoint(x: -1.1, y: -1.1)
        let positions = [IntVector(1,1)]
        let result = collisionDetector!.cropPathFrom(positions, vector: vector)
        XCTAssertEqual(result, CGPoint(x: -1, y: -1))
    }
    func testSlideBetweenColumns() {
        dropsContainer?.unarchiveBoardState(".... ..D. ..d.")
        let vector = CGPoint(x: 0.01, y: -1.2)
        let positions = [IntVector(2,4), IntVector(2,3)]
        let result = collisionDetector!.cropPathFrom(positions, vector: vector)
        XCTAssertEqual(result, CGPoint(x: 0.01, y: -1))
    }
    func testSlideBetweenColumns2() {
        dropsContainer?.unarchiveBoardState(".... ..D. ..d.")
        let vector = CGPoint(x: 0.7, y: -1.2)
        let positions = [IntVector(2,4), IntVector(2,3)]
        let result = collisionDetector!.cropPathFrom(positions, vector: vector)
        XCTAssertEqual(result, CGPoint(x: 1, y: -1.2))
    }
    func testSlideBetweenColumns3() {
        dropsContainer?.unarchiveBoardState(".... ..D. ..d.")
        let vector = CGPoint(x: 0.01, y: -1.2)
        let positions = [IntVector(1,4), IntVector(1,3)]
        let result = collisionDetector!.cropPathFrom(positions, vector: vector)
        XCTAssertEqual(result, CGPoint(x: 0, y: -1.2))
    }
    func testSlideBetweenColumns4() {
        dropsContainer?.unarchiveBoardState(".... ..D. ..d.")
        let vector = CGPoint(x: 0.7, y: -1.2)
        let positions = [IntVector(1,4), IntVector(1,3)]
        let result = collisionDetector!.cropPathFrom(positions, vector: vector)
        XCTAssertEqual(result, CGPoint(x: 0.7, y: -1))
    }
    func testSlideBetweenColumns5() {
        dropsContainer?.unarchiveBoardState(".... ..D. ..d.")
        let vector = CGPoint(x: -0.3, y: -1.1)
        let positions = [IntVector(3,4), IntVector(3,3)]
        let result = collisionDetector!.cropPathFrom(positions, vector: vector)
        XCTAssertEqual(result, CGPoint(x: 0, y: -1.1))
    }
}
