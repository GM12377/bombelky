//
//  GameControllerFactory.swift
//  bombelky
//
//  Created by Grzegorz Moscichowski on 30/05/16.
//  Copyright © 2016 Grzegorz Moscichowski. All rights reserved.
//

import Foundation

struct GameOptionsFactory {
    
    static func test6_12game() -> GameOptions {
        return GameOptions(boardSize: IntVector(6,12), randomColorsSeed:7, dropColorsNumber:4)
    }
    static func test4_4game() -> GameOptions {
        return GameOptions(boardSize: IntVector(4,4), randomColorsSeed:7, dropColorsNumber:6)
    }

}