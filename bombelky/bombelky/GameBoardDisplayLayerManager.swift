//
//  GameBoardDisplayLayerManager.swift
//  bombelky
//
//  Created by Grzegorz Moscichowski on 06/06/16.
//  Copyright © 2016 Grzegorz Moscichowski. All rights reserved.
//

import Foundation
import UIKit

protocol GameBoardDisplayLayerManager {
    weak var layer:CALayer? {get set}
    func layerBoundsChanged(_ bounds:CGRect)
}

class GameBoardDisplayLayerManagerImp: GameBoardDisplay, GameBoardDisplayLayerManager {
    var dropWidth:CGFloat?
    weak var layer:CALayer?

    init(layer:CALayer) {
        self.layer = layer
    }
    
    //MARK: GameBoardDisplayLayerManager
    func layerBoundsChanged(_ bounds:CGRect) {
        if let boardSize = boardSize {
            dropWidth = bounds.size.width / CGFloat(boardSize.x)
            return
        }
        dropWidth = nil
    }
    
    //MARK: GameBoardDisplay
    var boardSize:IntVector?
    func addDrop(_ drop: Drop) {
        let dropFrame = _dropFrameFrom(drop.position.cgPoint)
        let dropLayer = DropLayer(frame: dropFrame!,
                                  color: drop.color.UIColorRepresentation(),
                                  neighbours: drop.spillDirections)
        dropLayer.name = drop.tag
        if drop.star {
            dropLayer.addSublayer(StarLayer(frame: CGRect(origin: CGPoint.zero, size: dropFrame! .size)))
        }
        layer?.addSublayer(dropLayer)
    }
    fileprivate func _dropFrameFrom(_ position:CGPoint) -> CGRect? {
        if let dropWidth = dropWidth, let bounds = layer?.bounds {
            let origin = CGPoint(x: position.x * dropWidth,
                                 y: bounds.size.height - (1 + position.y) * dropWidth  )
            return CGRect(origin: origin, size: CGSize(width: dropWidth, height: dropWidth))
        }
        return nil
    }
    func removeDropByTag(_ tag: String?) {
        if let drop = _dropLayerFor(tag) {
            drop.removeFromSuperlayer()
        }
    }
    func moveDrop(_ tag: String?, to position: IntVector) {
        moveDrop(tag, to: position.cgPoint)
    }
    func moveDrop(_ tag: String?, to position: CGPoint) {
        if let drop = _dropLayerFor(tag) {
            drop.position = _dropFrameFrom(position)!.origin
        }
    }
    func refreshSpillForDrop(_ drop: Drop) {
        if let dropLayer = _dropLayerFor(drop.tag) {
            dropLayer.removeFromSuperlayer()
            addDrop(drop)
        }
    }
    fileprivate func _dropLayerFor(_ tag:String?) -> DropLayer? {
        if let container = layer?.sublayers {
            for drop in container {
                if let dropLayer = drop as? DropLayer {
                    if dropLayer.name == tag {
                        return dropLayer
                    }
                }
            }
        }
        return nil
    }
}
