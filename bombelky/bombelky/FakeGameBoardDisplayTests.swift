//
//  FakeGameBoardDisplayTests.swift
//  bombelky
//
//  Created by Grzegorz Moscichowski on 13/05/16.
//  Copyright © 2016 Grzegorz Moscichowski. All rights reserved.
//

import XCTest

class FakeGameBoardDisplayTests: XCTestCase {

    func testTagging() {
        let display = FakeGameBoardDisplay()
        var drop = Drop()
        drop.tag = "a"
        display.addDrop(drop)
        drop.tag = "b"
        display.addDrop(drop)
        XCTAssertEqual(display.addedDrops.count, 2)
        display.removeDropByTag("b")
        XCTAssertEqual(display.addedDrops.count, 1)
        display.removeDropByTag("b")
        XCTAssertEqual(display.addedDrops.count, 1)
    }
    
    func testSpilling() {
        let display = FakeGameBoardDisplay()
        var drop = Drop(tag:"a")
        display.addDrop(drop)
        drop.spillDirections = Set([.north])
        display.refreshSpillForDrop(drop)//   spillDrop("a", direction: .North)
        XCTAssertEqual(display.spillDirectionsOfDrop("b"), nil)
        XCTAssertEqual(display.spillDirectionsOfDrop("a"), Set([.north]))
        XCTAssertNotEqual(display.spillDirectionsOfDrop("a"), Set([.south]))
    }
    func testDescriptionGeneration() {
        let display = FakeGameBoardDisplay()
        display.boardSize = IntVector(3,3)
        display.addDrop(Drop(color: .colorC, position: IntVector(2,1), star:false, tag:"a"))
        display.addDrop(Drop(color: .colorA, position: IntVector(0,0), star:true, tag:"s"))
        XCTAssertEqual(display.generateDescription(), "... ..c A..")
    }
}
